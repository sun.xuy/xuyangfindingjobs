% This file is to use nerual network to perform inverse kinematics of
% quadruped Husky.
% First, generate the training data, given the trajectories(randomly),
% using fmincon to generate the joints angle, and then use the data to
% train neural network.



clear all;
close all;
clc;

params;
% N = 1e1;
% q1 = 1+1*rand(1,N);
% q2 = 0.7+0.4*rand(1,N);
% q3 = -0.05-0.04*rand(1,N);
q1 = 2.0439;
q2 = 1.1019;
q3 = -0.0504;





x0 = [2.0439 1.1019 0.0504 2.1434 1.3593 -0.0504  2.1434 1.3593 0.0504  ];

q = [q1;q2;q3];
% options = optimset('Display','off');
% for i = 1:N

% myfun = @constrainedQ;
F = @(x) constrainedQ(x,q);
output = fsolve(F,x0);

q4 = output(1);
q5 = output(2);
q6 = output(3);
q7 = output(4);
q8 = output(5);
q9 = output(6);
q10 = output(7);
q11 = output(8);
q12= output(9);


% end

x = [q1;q2;q3;q4;q5;q6;q7;q8;q9;q10;q11;q12];

for i = 1:N
[~,~,~,~,~,~,...
    p11(:,i),p12(:,i),p13(:,i),p14(:,i),p15(:,i),p16(:,i),p17(:,i),...
    p21(:,i),p22(:,i),p23(:,i),p24(:,i),p25(:,i),p26(:,i),p27(:,i),...
    p31(:,i),p32(:,i),p33(:,i),p34(:,i),p35(:,i),p36(:,i),p37(:,i),...
    p41(:,i),p42(:,i),p43(:,i),p44(:,i),p45(:,i),p46(:,i),p47(:,i),...
    pb1(:,i),pb2(:,i),pb3(:,i)] = func_fwd_kinematics(x(:,i));

end



for i =1:N
    
[pb1(:,i),~,~,~,~,~,~,~,~] = func_fwd_kinematics2(x(:,i));

end










