function [c12,c13,c14,c23,c24,c34,p11,p12,p13,p14,p15,p16,p17,p21,p22,p23,p24,p25,p26,p27,p31,p32,p33,p34,p35,p36,p37,p41,p42,p43,p44,p45,p46,p47,pb1,pb2,pb3] = func_fwd_kinematics(x)


params;

% rotation around z
Z = @(z,q)   [1       0          0       0;...
              0   cos(q)    -sin(q)      0;...
              0   sin(q)     cos(q)      z;...
              0       0          0       1] ;

% rotation around y
Y = @(y,q)       [   cos(q)     0     sin(q)  0 ;...
                         0      1         0   y;...
                    -sin(q)     0     cos(q)  0;...
                         0      0         0   1];
% translation matrix

T = @(x,y)   [  1   0   0   x;...
                0   1   0   y;...
                0   0   1   0;...
                0   0   0   1];




%%
%origin is the center of the square of the 4 feet
% transformation matrix from origin to zero point of the first leg
To10 = T(lwth/2,llth/2);
% transformation matrix from foot1 to hip drive

T100_pr = Y(0,x(3));
T10_pr1 = Z(0,pi/2-x(2));
T112 = Z(l1, -(pi-x(1)));
T123 = Z(l3,pi-x(1));
T135 = Z(l2,0);
T156 = Z(l4,-(pi/2-x(2)));
T167 = Y(-l5,-x(3));
T112_pr = Z(l1,0);
T12_pr4 = Z(l2,-(pi-x(1)));

% transformation matrix from hip in leg1 to hip in leg 2
T1727 = T(-lbrk,0);

% tansformation matrix from hip to feet in leg2
T276_pr = Y(l5,x(6));
T26_pr6 = Z(0,pi/2-x(5));
T265 = Z(-l4,0);
T253 = Z(-l2,-(pi-x(4)));
T232 = Z(-l3,pi-x(4));
T221 = Z(-l1,-(pi/2-x(5)));
T210 = Y(0,-x(6));

% tansformation matrix from foot in leg 2 to foot in leg 1
T2010 = T(lwth,0);

% set foot 1 a starting point, go through leg 2 and goes back to foot 1 again
% create a constraint for leg 1 and 2
C12 = T100_pr*T10_pr1*T112*T123*T135*T156*T167*T1727*T276_pr*T26_pr6*T265*T253*T232*T221*T210*T2010;
c12 = C12(1:3,4);


%%

% generate constraint from leg 3 to leg 4 
% transformation matrix from origin to zero point of the first leg
To30 = T(lwth/2,-llth/2);
% transformation matrix from foot1 to hip drive

T300_pr = Y(0,x(9));
T30_pr1 = Z(0,pi/2-x(8));
T312 = Z(l1, -(pi-x(7)));
T323 = Z(l3,pi-x(7));
T335 = Z(l2,0);
T356 = Z(l4,-(pi/2-x(8)));
T367 = Y(-l5,-x(9));

T312_pr = Z(l1,0);
T32_pr4 = Z(l2,-(pi-x(7)));

% transformation matrix from hip in leg1 to hip in leg 2
T3747 = T(-lbrk,0);
% tansformation matrix from hip to feet in leg2
T476_pr = Y(l5,x(12));
T46_pr6 = Z(0,pi/2-x(11));
T465 = Z(-l4,0);
T453 = Z(-l2,-(pi-x(10)));
T432 = Z(-l3,pi-x(10));
T421 = Z(-l1,-(pi/2-x(11)));
T410 = Y(0,-x(12));

% tansformation matrix from foot in leg 4 to foot in leg 2
T4030 = T(lwth,0);

% set foot 3 a starting point, go through leg 4 and goes back to foot 3 again
% create a constraint for leg 3 and 4
C34 = T300_pr*T30_pr1*T312*T323*T335*T356*T367*T3747*T476_pr*T46_pr6*T465*T453*T432*T421*T410*T4030;
c34 = C34(1:3,4);

%%

% generate constraint between leg 1 and leg 3
% transformation matrix from foot1 to hip drive

% T100_pr = Y(0,0,x(3));
% T10_pr1 = Z(0,pi/2-x(2));
% T112 = Z(l1, -(pi-x(1)));
% T123 = Z(l3,pi-x(1));
% T135 = Z(l2,0);
% T156 = Z(l4,-(pi/2-x(2)));
% T167 = Y(0,-l5,-x(3));
          
% transformation matrix from hip 1 to hip3

T1737 = T(0,-lbb);

% transformation matrix from hip to feet in leg 3
T376_pr = Y(l5,x(9));
T36_pr6 = Z(0,pi/2-x(8));
T365 = Z(-l4,0);
T353 = Z(-l2,-(pi-x(7)));
T332 = Z(-l3,pi-x(7));
T321 = Z(-l1,-(pi/2-x(8)));
T310 = Y(0,-x(9));

% tansformation matrix from foot in leg 3 to foot in leg 1
T3010 = T(0,llth);

% set foot 1 a starting point, go through leg 3 and goes back to foot 1 again
% generate a constraint between leg 1 and 3

C13 = T100_pr*T10_pr1*T112*T123*T135*T156*T167*T1737*T376_pr*T36_pr6*T365*T353*T332*T321*T310*T3010;
c13 = C13(1:3,4); 
%%


% generate constraint between leg 2 and leg 4
% transformation matrix from foot1 to hip drive
To20 = T(-lwth/2,llth/2);
T200_pr = Y(0,x(6));
T20_pr1 = Z(0,pi/2-x(5));
T212 = Z(l1, -(pi-x(4)));
T223 = Z(l3,pi-x(4));
T235 = Z(l2,0);
T256 = Z(l4,-(pi/2-x(5)));
T267 = Y(-l5,-x(6));

T212_pr = Z(l1,0);
T22_pr4 = Z(l2,-(pi-x(4)));

          
% transformation matrix from hip 2 to hip4

T2747 = T(0,-lbb);

% transformation matrix from hip to feet in leg 4
% T476_pr = Y(0,l5,x(12));
% T46_pr6 = Z(0,pi/2-x(11));
% T465 = Z(-l4,0);
% T453 = Z(-l2,-(pi-x(10)));
% T432 = Z(-l3,pi-x(10));
% T421 = Z(-l1,-(pi/2-x(11)));
% T410 = Y(0,0,-x(12));

% tansformation matrix from foot in leg 4 to foot in leg 2
T4020 = T(0,llth);

% set foot 2 a starting point, go through leg 4 and goes back to foot 1 again
% generate a constraint between leg 2 and 4

C24 = T200_pr*T20_pr1*T212*T223*T235*T256*T267*T2747*T476_pr*T46_pr6*T465*T453*T432*T421*T410*T4020;
c24 = C24(1:3,4); 





%%
% generate constraint between leg 1 and leg 4
% transformation matrix from foot1 to hip drive

% T100_pr = Y(0,x(3));
% T10_pr1 = Z(0,pi/2-x(2));
% T112 = Z(l1, -(pi-x(1)));
% T123 = Z(l3,pi-x(1));
% T135 = Z(l2,0);
% T156 = Z(l4,-(pi/2-x(2)));
% T167 = Y(-l5,-x(3));
          
% transformation matrix from hip 1 to hip4

T1747 = T(-lbrk,-lbb);

% % transformation matrix from hip to feet in leg 3
% T476_pr = Y(l5,x(12));
% T46_pr6 = Z(0,pi/2-x(11));
% T465 = Z(-l4,0);
% T453 = Z(-l2,-(pi-x(10)));
% T432 = Z(-l3,pi-x(10));
% T421 = Z(-l1,-(pi/2-x(11)));
% T410 = Y(0,-x(12));

% tansformation matrix from foot in leg 4 to foot in leg 1
T4010 = T(lwth,llth);

% generate constraints between leg 1 and leg 4
C14 = T100_pr*T10_pr1*T112*T123*T135*T156*T167*T1747*T476_pr*T46_pr6*T465*T453*T432*T421*T410*T4010;
c14 = C14(1:3,4); 

%%

% generate constraint between leg 2 and leg 3
% transformation matrix from foot1 to hip drive

% T200_pr = Y(0,x(6));
% T20_pr1 = Z(0,pi/2-x(5));
% T212 = Z(l1, -(pi-x(4)));
% T223 = Z(l3,pi-x(4));
% T235 = Z(l2,0);
% T256 = Z(l4,-(pi/2-x(5)));
% T267 = Y(-l5,-x(6));
%           
          
% transformation matrix from hip 2 to hip3

T2737 = T(lbrk,-lbb);


% % transformation matrix from hip to feet in leg 3
% 
% T376_pr = Y(l5,x(9));
% T36_pr6 = Z(0,pi/2-x(8));
% T365 = Z(-l4,0);
% T353 = Z(-l2,-(pi-x(7)));
% T332 = Z(-l3,pi-x(7));
% T321 = Z(-l1,-(pi/2-x(8)));
% T310 = Y(0,-x(9));


% tansformation matrix from foot in leg 2 to foot in leg 3
T3020 = T(-lwth,llth);

% generate constraints between leg 2 and leg 3
C23 = T200_pr*T20_pr1*T212*T223*T235*T256*T267*T2737*T376_pr*T36_pr6*T365*T353*T332*T321*T310*T3020;
c23 = C23(1:3,4);


% from feet to hip in leg 4
To40 = T(-lwth/2,-llth/2);

T400_pr = Y(0,x(12));
T40_pr1 = Z(0,pi/2-x(11));
T412 = Z(l1, -(pi-x(10)));
T423 = Z(l3,pi-x(10));
T435 = Z(l2,0);
T456 = Z(l4,-(pi/2-x(11)));
T467 = Y(-l5,-x(12));

T412_pr = Z(l1,0);
T42_pr4 = Z(l2,-(pi-x(10)));

% transformation matrix from origin to pb1
Tpb1 = T(lbrk/2,-lbb/2);
TTpb1 =  To20*T200_pr*T20_pr1*T212*T223*T235*T256*T267*Tpb1;
pb1 = TTpb1(1:3,4);

% transformation matrix from origin to pb2
Tpb2 = T(-lbrk/2-sftx_traj,-lbb/2-sfty_traj);
TTpb2 =  To10*T100_pr*T10_pr1*T112*T123*T135*T156*T167*Tpb2;
pb2 = TTpb2(1:3,4);

% transformation matrix from origin to pb3
Tpb3 = T(-lbrk/2+sftx_traj,-lbb/2+sfty_traj);
TTpb3 =  To10*T100_pr*T10_pr1*T112*T123*T135*T156*T167*Tpb3;
pb3 = TTpb3(1:3,4);

% position of all points in leg1 
P11 =To10;
p11 = P11(1:3,4);
P12 = To10*T100_pr*T10_pr1*T112;
p12 = P12(1:3,4);
P13 = To10*T100_pr*T10_pr1*T112*T123;
p13 = P13(1:3,4);
P14 = To10*T100_pr*T10_pr1*T112_pr*T12_pr4;
p14 = P14(1:3,4);
P15 = To10*T100_pr*T10_pr1*T112*T123*T135;
p15 = P15(1:3,4);
P16  = To10*T100_pr*T10_pr1*T112*T123*T135*T156;
p16 = P16(1:3,4);
P17 = To10*T100_pr*T10_pr1*T112*T123*T135*T156*T167;
p17 = P17(1:3,4);

% position of all points in leg2 
P21 =To20;
p21 = P21(1:3,4);
P22 = To20*T200_pr*T20_pr1*T212;
p22 = P22(1:3,4);
P23 = To20*T200_pr*T20_pr1*T212*T223;
p23 = P23(1:3,4);
P24 = To20*T200_pr*T20_pr1*T212_pr*T22_pr4;
p24 = P24(1:3,4);
P25 = To20*T200_pr*T20_pr1*T212*T223*T235;
p25 = P25(1:3,4);
P26  =To20*T200_pr*T20_pr1*T212*T223*T235*T256;
p26 = P26(1:3,4);
P27 = To20*T200_pr*T20_pr1*T212*T223*T235*T256*T267;
p27 = P27(1:3,4);

% position of all points in leg3
P31 =To30;
p31 = P31(1:3,4);
P32 = To30*T300_pr*T30_pr1*T312;
p32 = P32(1:3,4);
P33 = To30*T300_pr*T30_pr1*T312*T323;
p33 = P33(1:3,4);
P34 = To30*T300_pr*T30_pr1*T312_pr*T32_pr4;
p34 = P34(1:3,4);
P35 = To30*T300_pr*T30_pr1*T312*T323*T335;
p35 = P35(1:3,4);
P36  =To30*T300_pr*T30_pr1*T312*T323*T335*T356;
p36 = P36(1:3,4);
P37 = To30*T300_pr*T30_pr1*T312*T323*T335*T356*T367;
p37 = P37(1:3,4);

% position of all points in leg4 
P41 =To40;
p41 = P41(1:3,4);
P42 = To40*T400_pr*T40_pr1*T412;
p42 = P42(1:3,4);
P43 = To40*T400_pr*T40_pr1*T412*T423;
p43 = P43(1:3,4);
P44 = To40*T400_pr*T40_pr1*T412_pr*T42_pr4;
p44 = P44(1:3,4);
P45 = To40*T400_pr*T40_pr1*T412*T423*T435;
p45 = P45(1:3,4);
P46  =To40*T400_pr*T40_pr1*T412*T423*T435*T456;
p46 = P46(1:3,4);
P47 = To40*T400_pr*T40_pr1*T412*T423*T435*T456*T467;
p47 = P47(1:3,4);

end 

