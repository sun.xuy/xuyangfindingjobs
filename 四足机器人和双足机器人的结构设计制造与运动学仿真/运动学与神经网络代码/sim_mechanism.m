
% the file is to solve inverse kinematics problems, meaning that we need to make the QP-midget to 
% follow trajectory of 3 points on the back bone, which is parallel to the ground. One leg has 3 
% variables, in total we have 12 varibles in QP-midge.

% refer to notes
% 3 variables in each leg, 12 in total
% q23 means q3 in leg 2
% x = [q11 q12 q13 q21 q22 q23 q31 q32 q33 q41 q42 q43 llth lwth];

clear all;
close all;
clc;

% obtain params from CAD model
params;

% set trajectory
% follow three points in the spine
n = 20;
pb = linspace(0.595,0.33,n);

for i = 1:n
pb1_traj(i,:) = [0,0,pb(i)];
pb2_traj(i,:) = [-sftx_traj,-sfty_traj,pb(i)];
pb3_traj(i,:) = [sftx_traj,sfty_traj,pb(i)];
end


% create initial guess
w = [130 60 0]*pi/180;
% x0 = [w w w w 0.55*180/pi 0.15*180/pi]*pi/180;
x0 = [w w w w];

% obtain q in each leg
for i = 1:n

x(i,:) = func_getQ(x0,pb1_traj(i,:));
% set new initial guess for next time step
x0 = x(i,:);

end

%%
% use forward kinematics to calculate all the positions of each joint in
% the robot


for i = 1:n
    

    [~,~,~,~,~,~,...
     p11(i,:),p12(i,:),p13(i,:),p14(i,:),p15(i,:),p16(i,:),p17(i,:),...
     p21(i,:),p22(i,:),p23(i,:),p24(i,:),p25(i,:),p26(i,:),p27(i,:)...
     p31(i,:),p32(i,:),p33(i,:),p34(i,:),p35(i,:),p36(i,:),p37(i,:)...
     p41(i,:),p42(i,:),p43(i,:),p44(i,:),p45(i,:),p46(i,:),p47(i,:)...
     pb1(i,:),pb2(i,:),pb3(i,:)] = func_fwd_kinematics(x(i,:));
    
    
end

%%
% calculate the hip position after sitting down
p17expand = p17(end,:);
p27expand = p27(end,:);
p37expand = p37(end,:);
p47expand = p47(end,:);

%% 
% expand the leg depending on the the distance between foot1 and foot2 & between foot3 and foot4

% create a trajectories for the leg to expand




y = zeros(2*n,12);
y(:,1) = [x(:,1);x(end,1)*ones(n,1)];
y(:,2) = [x(:,2);x(end,2)*ones(n,1)];
y(:,3) = [x(:,3);(linspace(x(end,3),-90/180*3.14,n)).'];
y(:,4) = [x(:,4);x(end,4)*ones(n,1)];
y(:,5) = [x(:,5);x(end,5)*ones(n,1)];
y(:,6) = [x(:,6);(linspace(x(end,6),90/180*3.14,n)).'];
y(:,7) = [x(:,7);x(end,7)*ones(n,1)];
y(:,8) = [x(:,8);x(end,8)*ones(n,1)];
y(:,9) = [x(:,9);(linspace(x(end,9),-90/180*3.14,n)).'];
y(:,10) = [x(:,10);x(end,10)*ones(n,1)];
y(:,11) = [x(:,11);x(end,11)*ones(n,1)];
y(:,12) = [x(:,12);(linspace(x(end,12),90/180*3.14,n)).'];

x = y;


for i = 1:n
%expand the leg
    [p11(n+i,:),p12(n+i,:),p13(n+i,:),p14(n+i,:),p15(n+i,:),p16(n+i,:),p17(n+i,:),...
     p21(n+i,:),p22(n+i,:),p23(n+i,:),p24(n+i,:),p25(n+i,:),p26(n+i,:),p27(n+i,:)...
     p31(n+i,:),p32(n+i,:),p33(n+i,:),p34(n+i,:),p35(n+i,:),p36(n+i,:),p37(n+i,:)...
     p41(n+i,:),p42(n+i,:),p43(n+i,:),p44(n+i,:),p45(n+i,:),p46(n+i,:),p47(n+i,:)...
     ] = func_fwd_expand(x(n+i,:),p17expand,p27expand,p37expand,p47expand);

end
%%
% set the auxiliary line
h = 0.02;
hspline = p17(n,3);

p171 = p17 + [0 0 -h];
p271 = p27 + [0 0 -h];
p371 = p37 + [0 0 -h];
p471 = p47 + [0 0 -h];
p127 = (p17+p27)./2;
p347 = (p37+p47)./2;
pmdl = (p127+p347)./2';
pbtm = [pmdl(:,1:2) pmdl(:,3)-hspline];
pb1 = [pb1;pb1(end,:).*ones(n,3)];
pb2 = [pb2;pb2(end,:).*ones(n,3)];
pb3 = [pb3;pb3(end,:).*ones(n,3)];





filename = 'sim_mechanism.gif';
for i = 1:n*2
    
% plot 3d of the robot
    ax10 = subplot(2,2,1:2);
% %     plot the trajectory
    ax_traj = plot3([pb1_traj(1,1),pb1_traj(end,1)],[pb1_traj(1,2),pb1_traj(end,2)],[pb1_traj(1,3),pb1_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    hold on
    plot3([pb2_traj(1,1),pb2_traj(end,1)],[pb2_traj(1,2),pb2_traj(end,2)],[pb2_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    plot3([pb3_traj(1,1),pb3_traj(end,1)],[pb3_traj(1,2),pb3_traj(end,2)],[pb3_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    
    
% %     plot points on the spine
    ax_follow = plot3(pb1(i,1),pb1(i,2),pb1(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
    plot3(pb2(i,1),pb2(i,2),pb2(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
    plot3(pb3(i,1),pb3(i,2),pb3(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);

% plot first leg
    
    
    ax11 = plot3([p11(i,1),p12(i,1)],[p11(i,2),p12(i,2)],[p11(i,3),p12(i,3)],'color','k');
%     hold on
    plot3([p12(i,1),p14(i,1)],[p12(i,2),p14(i,2)],[p12(i,3),p14(i,3)],'color','k');
    plot3([p12(i,1),p13(i,1)],[p12(i,2),p13(i,2)],[p12(i,3),p13(i,3)],'color','k');
    plot3([p13(i,1),p15(i,1)],[p13(i,2),p15(i,2)],[p13(i,3),p15(i,3)],'color','k');
    plot3([p14(i,1),p15(i,1)],[p14(i,2),p15(i,2)],[p14(i,3),p15(i,3)],'color','k');
    plot3([p15(i,1),p16(i,1)],[p15(i,2),p16(i,2)],[p15(i,3),p16(i,3)],'color','k');
    plot3([p16(i,1),p17(i,1)],[p16(i,2),p17(i,2)],[p16(i,3),p17(i,3)],'color','k');
    
    plot3(p11(i,1),p11(i,2),p11(i,3),'o','color','k','MarkerSize',5);
    plot3(p12(i,1),p12(i,2),p12(i,3),'o','color','k','MarkerSize',5);
    plot3(p13(i,1),p13(i,2),p13(i,3),'o','color','k','MarkerSize',5);
    plot3(p14(i,1),p14(i,2),p14(i,3),'o','color','k','MarkerSize',5);
    plot3(p15(i,1),p15(i,2),p15(i,3),'o','color','k','MarkerSize',5);
    plot3(p16(i,1),p16(i,2),p16(i,3),'o','color','k','MarkerSize',5);
    plot3(p17(i,1),p17(i,2),p17(i,3),'o','color','k','MarkerSize',5);
    

% plot second leg
    ax12 = plot3([p21(i,1),p22(i,1)],[p21(i,2),p22(i,2)],[p21(i,3),p22(i,3)],'color','m');
 
    plot3([p22(i,1),p24(i,1)],[p22(i,2),p24(i,2)],[p22(i,3),p24(i,3)],'color','m');
    plot3([p22(i,1),p23(i,1)],[p22(i,2),p23(i,2)],[p22(i,3),p23(i,3)],'color','m');
    plot3([p23(i,1),p25(i,1)],[p23(i,2),p25(i,2)],[p23(i,3),p25(i,3)],'color','m');
    plot3([p24(i,1),p25(i,1)],[p24(i,2),p25(i,2)],[p24(i,3),p25(i,3)],'color','m');
    plot3([p25(i,1),p26(i,1)],[p25(i,2),p26(i,2)],[p25(i,3),p26(i,3)],'color','m');
    plot3([p26(i,1),p27(i,1)],[p26(i,2),p27(i,2)],[p26(i,3),p27(i,3)],'color','m');
    
    plot3(p21(i,1),p21(i,2),p21(i,3),'o','color','m','MarkerSize',5);
    plot3(p22(i,1),p22(i,2),p22(i,3),'o','color','m','MarkerSize',5);
    plot3(p23(i,1),p23(i,2),p23(i,3),'o','color','m','MarkerSize',5);
    plot3(p24(i,1),p24(i,2),p24(i,3),'o','color','m','MarkerSize',5);
    plot3(p25(i,1),p25(i,2),p25(i,3),'o','color','m','MarkerSize',5);
    plot3(p26(i,1),p26(i,2),p26(i,3),'o','color','m','MarkerSize',5);
    plot3(p27(i,1),p27(i,2),p27(i,3),'o','color','m','MarkerSize',5);
    
     


% plot third leg
    ax13 = plot3([p31(i,1),p32(i,1)],[p31(i,2),p32(i,2)],[p31(i,3),p32(i,3)],'color','g');
 
    plot3([p32(i,1),p34(i,1)],[p32(i,2),p34(i,2)],[p32(i,3),p34(i,3)],'color','g');
    plot3([p32(i,1),p33(i,1)],[p32(i,2),p33(i,2)],[p32(i,3),p33(i,3)],'color','g');
    plot3([p33(i,1),p35(i,1)],[p33(i,2),p35(i,2)],[p33(i,3),p35(i,3)],'color','g');
    plot3([p34(i,1),p35(i,1)],[p34(i,2),p35(i,2)],[p34(i,3),p35(i,3)],'color','g');
    plot3([p35(i,1),p36(i,1)],[p35(i,2),p36(i,2)],[p35(i,3),p36(i,3)],'color','g');
    plot3([p36(i,1),p37(i,1)],[p36(i,2),p37(i,2)],[p36(i,3),p37(i,3)],'color','g');
    
    plot3(p31(i,1),p31(i,2),p31(i,3),'o','color','g','MarkerSize',5);
    plot3(p32(i,1),p32(i,2),p32(i,3),'o','color','g','MarkerSize',5);
    plot3(p33(i,1),p33(i,2),p33(i,3),'o','color','g','MarkerSize',5);
    plot3(p34(i,1),p34(i,2),p34(i,3),'o','color','g','MarkerSize',5);
    plot3(p35(i,1),p35(i,2),p35(i,3),'o','color','g','MarkerSize',5);
    plot3(p36(i,1),p36(i,2),p36(i,3),'o','color','g','MarkerSize',5);
    plot3(p37(i,1),p37(i,2),p37(i,3),'o','color','g','MarkerSize',5);
    
 

% plot fourth leg
    ax14 = plot3([p41(i,1),p42(i,1)],[p41(i,2),p42(i,2)],[p41(i,3),p42(i,3)],'color','c');

    plot3([p42(i,1),p44(i,1)],[p42(i,2),p44(i,2)],[p42(i,3),p44(i,3)],'color','c');
    plot3([p42(i,1),p43(i,1)],[p42(i,2),p43(i,2)],[p42(i,3),p43(i,3)],'color','c');
    plot3([p43(i,1),p45(i,1)],[p43(i,2),p45(i,2)],[p43(i,3),p45(i,3)],'color','c');
    plot3([p44(i,1),p45(i,1)],[p44(i,2),p45(i,2)],[p44(i,3),p45(i,3)],'color','c');
    plot3([p45(i,1),p46(i,1)],[p45(i,2),p46(i,2)],[p45(i,3),p46(i,3)],'color','c');
    plot3([p46(i,1),p47(i,1)],[p46(i,2),p47(i,2)],[p46(i,3),p47(i,3)],'color','c');
    
    plot3(p41(i,1),p41(i,2),p41(i,3),'o','color','c','MarkerSize',5);
    plot3(p42(i,1),p42(i,2),p42(i,3),'o','color','c','MarkerSize',5);
    plot3(p43(i,1),p43(i,2),p43(i,3),'o','color','c','MarkerSize',5);
    plot3(p44(i,1),p44(i,2),p44(i,3),'o','color','c','MarkerSize',5);
    plot3(p45(i,1),p45(i,2),p45(i,3),'o','color','c','MarkerSize',5);
    plot3(p46(i,1),p46(i,2),p46(i,3),'o','color','c','MarkerSize',5);
    plot3(p47(i,1),p47(i,2),p47(i,3),'o','color','c','MarkerSize',5);
    
    
    
% % plot spline2
ps = plot3([p17(i,1),p27(i,1)],[p17(i,2),p27(i,2)],[p17(i,3),p27(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p47(i,1),p37(i,1)],[p47(i,2),p37(i,2)],[p47(i,3),p37(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p127(i,1),p347(i,1)],[p127(i,2),p347(i,2)],[p127(i,3),p347(i,3)],'color',[0 0.4470 0.7410]);
belly = plot3([pmdl(i,1),pbtm(i,1)],[pmdl(i,2),pbtm(i,2)],[pmdl(i,3),pbtm(i,3)],'color',[0.8706    0.3725    0.1569]);
    
    
    
    

    hold off
view(ax10,[17,10,7]);
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.4 0.4]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
title(ax10,'3D View');
legend('boxoff');
legend([ax_traj, ax_follow,ax11,ax12,ax13,ax14,ps,belly],'trajectory',['following-trajectory' newline 'points on the robot'],'1st leg','2nd leg','3rd leg','4th leg','spine','belly');
box on





%%
% plot the side view
ax30 = subplot(2,2,3);
% %     plot the trajectory
    ax3_traj = plot3([pb1_traj(1,1),pb1_traj(end,1)],[pb1_traj(1,2),pb1_traj(end,2)],[pb1_traj(1,3),pb1_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    hold on
    plot3([pb2_traj(1,1),pb2_traj(end,1)],[pb2_traj(1,2),pb2_traj(end,2)],[pb2_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    plot3([pb3_traj(1,1),pb3_traj(end,1)],[pb3_traj(1,2),pb3_traj(end,2)],[pb3_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    
    
% %     plot points on the spine
    ax3_follow = plot3(pb1(i,1),pb1(i,2),pb1(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
    plot3(pb2(i,1),pb2(i,2),pb2(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
    plot3(pb3(i,1),pb3(i,2),pb3(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
   
    
    ax31 = plot3([p11(i,1),p12(i,1)],[p11(i,2),p12(i,2)],[p11(i,3),p12(i,3)],'color','k');
    hold on
    plot3([p12(i,1),p14(i,1)],[p12(i,2),p14(i,2)],[p12(i,3),p14(i,3)],'color','k');
    plot3([p12(i,1),p13(i,1)],[p12(i,2),p13(i,2)],[p12(i,3),p13(i,3)],'color','k');
    plot3([p13(i,1),p15(i,1)],[p13(i,2),p15(i,2)],[p13(i,3),p15(i,3)],'color','k');
    plot3([p14(i,1),p15(i,1)],[p14(i,2),p15(i,2)],[p14(i,3),p15(i,3)],'color','k');
    plot3([p15(i,1),p16(i,1)],[p15(i,2),p16(i,2)],[p15(i,3),p16(i,3)],'color','k');
    plot3([p16(i,1),p17(i,1)],[p16(i,2),p17(i,2)],[p16(i,3),p17(i,3)],'color','k');
    
    plot3(p11(i,1),p11(i,2),p11(i,3),'o','color','k','MarkerSize',5);
    plot3(p12(i,1),p12(i,2),p12(i,3),'o','color','k','MarkerSize',5);
    plot3(p13(i,1),p13(i,2),p13(i,3),'o','color','k','MarkerSize',5);
    plot3(p14(i,1),p14(i,2),p14(i,3),'o','color','k','MarkerSize',5);
    plot3(p15(i,1),p15(i,2),p15(i,3),'o','color','k','MarkerSize',5);
    plot3(p16(i,1),p16(i,2),p16(i,3),'o','color','k','MarkerSize',5);
    plot3(p17(i,1),p17(i,2),p17(i,3),'o','color','k','MarkerSize',5);
    

% plot second leg
    ax32 = plot3([p21(i,1),p22(i,1)],[p21(i,2),p22(i,2)],[p21(i,3),p22(i,3)],'color','m');
 
    plot3([p22(i,1),p24(i,1)],[p22(i,2),p24(i,2)],[p22(i,3),p24(i,3)],'color','m');
    plot3([p22(i,1),p23(i,1)],[p22(i,2),p23(i,2)],[p22(i,3),p23(i,3)],'color','m');
    plot3([p23(i,1),p25(i,1)],[p23(i,2),p25(i,2)],[p23(i,3),p25(i,3)],'color','m');
    plot3([p24(i,1),p25(i,1)],[p24(i,2),p25(i,2)],[p24(i,3),p25(i,3)],'color','m');
    plot3([p25(i,1),p26(i,1)],[p25(i,2),p26(i,2)],[p25(i,3),p26(i,3)],'color','m');
    plot3([p26(i,1),p27(i,1)],[p26(i,2),p27(i,2)],[p26(i,3),p27(i,3)],'color','m');
    
    plot3(p21(i,1),p21(i,2),p21(i,3),'o','color','m','MarkerSize',5);
    plot3(p22(i,1),p22(i,2),p22(i,3),'o','color','m','MarkerSize',5);
    plot3(p23(i,1),p23(i,2),p23(i,3),'o','color','m','MarkerSize',5);
    plot3(p24(i,1),p24(i,2),p24(i,3),'o','color','m','MarkerSize',5);
    plot3(p25(i,1),p25(i,2),p25(i,3),'o','color','m','MarkerSize',5);
    plot3(p26(i,1),p26(i,2),p26(i,3),'o','color','m','MarkerSize',5);
    plot3(p27(i,1),p27(i,2),p27(i,3),'o','color','m','MarkerSize',5);
    
     


% plot third leg
    ax33 = plot3([p31(i,1),p32(i,1)],[p31(i,2),p32(i,2)],[p31(i,3),p32(i,3)],'color','g');
 
    plot3([p32(i,1),p34(i,1)],[p32(i,2),p34(i,2)],[p32(i,3),p34(i,3)],'color','g');
    plot3([p32(i,1),p33(i,1)],[p32(i,2),p33(i,2)],[p32(i,3),p33(i,3)],'color','g');
    plot3([p33(i,1),p35(i,1)],[p33(i,2),p35(i,2)],[p33(i,3),p35(i,3)],'color','g');
    plot3([p34(i,1),p35(i,1)],[p34(i,2),p35(i,2)],[p34(i,3),p35(i,3)],'color','g');
    plot3([p35(i,1),p36(i,1)],[p35(i,2),p36(i,2)],[p35(i,3),p36(i,3)],'color','g');
    plot3([p36(i,1),p37(i,1)],[p36(i,2),p37(i,2)],[p36(i,3),p37(i,3)],'color','g');
    
    plot3(p31(i,1),p31(i,2),p31(i,3),'o','color','g','MarkerSize',5);
    plot3(p32(i,1),p32(i,2),p32(i,3),'o','color','g','MarkerSize',5);
    plot3(p33(i,1),p33(i,2),p33(i,3),'o','color','g','MarkerSize',5);
    plot3(p34(i,1),p34(i,2),p34(i,3),'o','color','g','MarkerSize',5);
    plot3(p35(i,1),p35(i,2),p35(i,3),'o','color','g','MarkerSize',5);
    plot3(p36(i,1),p36(i,2),p36(i,3),'o','color','g','MarkerSize',5);
    plot3(p37(i,1),p37(i,2),p37(i,3),'o','color','g','MarkerSize',5);
    
 

% plot fourth leg
    ax34 = plot3([p41(i,1),p42(i,1)],[p41(i,2),p42(i,2)],[p41(i,3),p42(i,3)],'color','c');

    plot3([p42(i,1),p44(i,1)],[p42(i,2),p44(i,2)],[p42(i,3),p44(i,3)],'color','c');
    plot3([p42(i,1),p43(i,1)],[p42(i,2),p43(i,2)],[p42(i,3),p43(i,3)],'color','c');
    plot3([p43(i,1),p45(i,1)],[p43(i,2),p45(i,2)],[p43(i,3),p45(i,3)],'color','c');
    plot3([p44(i,1),p45(i,1)],[p44(i,2),p45(i,2)],[p44(i,3),p45(i,3)],'color','c');
    plot3([p45(i,1),p46(i,1)],[p45(i,2),p46(i,2)],[p45(i,3),p46(i,3)],'color','c');
    plot3([p46(i,1),p47(i,1)],[p46(i,2),p47(i,2)],[p46(i,3),p47(i,3)],'color','c');
    
    plot3(p41(i,1),p41(i,2),p41(i,3),'o','color','c','MarkerSize',5);
    plot3(p42(i,1),p42(i,2),p42(i,3),'o','color','c','MarkerSize',5);
    plot3(p43(i,1),p43(i,2),p43(i,3),'o','color','c','MarkerSize',5);
    plot3(p44(i,1),p44(i,2),p44(i,3),'o','color','c','MarkerSize',5);
    plot3(p45(i,1),p45(i,2),p45(i,3),'o','color','c','MarkerSize',5);
    plot3(p46(i,1),p46(i,2),p46(i,3),'o','color','c','MarkerSize',5);
    plot3(p47(i,1),p47(i,2),p47(i,3),'o','color','c','MarkerSize',5);
    

    
    
% % plot spline2
ps = plot3([p17(i,1),p27(i,1)],[p17(i,2),p27(i,2)],[p17(i,3),p27(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p47(i,1),p37(i,1)],[p47(i,2),p37(i,2)],[p47(i,3),p37(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p127(i,1),p347(i,1)],[p127(i,2),p347(i,2)],[p127(i,3),p347(i,3)],'color',[0 0.4470 0.7410]);
    belly = plot3([pmdl(i,1),pbtm(i,1)],[pmdl(i,2),pbtm(i,2)],[pmdl(i,3),pbtm(i,3)],'color',[0 0.4470 0.7410]);
    
    
    hold off
view(ax30,[10,0,0]);
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.4 0.4]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
title(ax30,'Side View');
legend('boxoff');
legend([ax3_traj, ax3_follow,ax31,ax32,ax33,ax34,ps,belly],'trajectory',['following-trajectory' newline 'points on the robot'],'1st leg','2nd leg','3rd leg','4th leg','spine','belly');
box on

%%
 %-----------------------------------------------------------------------------
% plot front view
ax40 = subplot(2,2,4);

% %     plot the trajectory
    ax4_traj = plot3([pb1_traj(1,1),pb1_traj(end,1)],[pb1_traj(1,2),pb1_traj(end,2)],[pb1_traj(1,3),pb1_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    hold on
    plot3([pb2_traj(1,1),pb2_traj(end,1)],[pb2_traj(1,2),pb2_traj(end,2)],[pb2_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    plot3([pb3_traj(1,1),pb3_traj(end,1)],[pb3_traj(1,2),pb3_traj(end,2)],[pb3_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    
    
% %     plot points on the spine
    ax4_follow = plot3(pb1(i,1),pb1(i,2),pb1(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
    plot3(pb2(i,1),pb2(i,2),pb2(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
    plot3(pb3(i,1),pb3(i,2),pb3(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
   
    ax41 = plot3([p11(i,1),p12(i,1)],[p11(i,2),p12(i,2)],[p11(i,3),p12(i,3)],'color','k');
    hold on 
    plot3([p12(i,1),p14(i,1)],[p12(i,2),p14(i,2)],[p12(i,3),p14(i,3)],'color','k');
    plot3([p12(i,1),p13(i,1)],[p12(i,2),p13(i,2)],[p12(i,3),p13(i,3)],'color','k');
    plot3([p13(i,1),p15(i,1)],[p13(i,2),p15(i,2)],[p13(i,3),p15(i,3)],'color','k');
    plot3([p14(i,1),p15(i,1)],[p14(i,2),p15(i,2)],[p14(i,3),p15(i,3)],'color','k');
    plot3([p15(i,1),p16(i,1)],[p15(i,2),p16(i,2)],[p15(i,3),p16(i,3)],'color','k');
    plot3([p16(i,1),p17(i,1)],[p16(i,2),p17(i,2)],[p16(i,3),p17(i,3)],'color','k');
    
    plot3(p11(i,1),p11(i,2),p11(i,3),'o','color','k','MarkerSize',5);
    plot3(p12(i,1),p12(i,2),p12(i,3),'o','color','k','MarkerSize',5);
    plot3(p13(i,1),p13(i,2),p13(i,3),'o','color','k','MarkerSize',5);
    plot3(p14(i,1),p14(i,2),p14(i,3),'o','color','k','MarkerSize',5);
    plot3(p15(i,1),p15(i,2),p15(i,3),'o','color','k','MarkerSize',5);
    plot3(p16(i,1),p16(i,2),p16(i,3),'o','color','k','MarkerSize',5);
    plot3(p17(i,1),p17(i,2),p17(i,3),'o','color','k','MarkerSize',5);
    

% plot second leg
    ax42 = plot3([p21(i,1),p22(i,1)],[p21(i,2),p22(i,2)],[p21(i,3),p22(i,3)],'color','m');
 
    plot3([p22(i,1),p24(i,1)],[p22(i,2),p24(i,2)],[p22(i,3),p24(i,3)],'color','m');
    plot3([p22(i,1),p23(i,1)],[p22(i,2),p23(i,2)],[p22(i,3),p23(i,3)],'color','m');
    plot3([p23(i,1),p25(i,1)],[p23(i,2),p25(i,2)],[p23(i,3),p25(i,3)],'color','m');
    plot3([p24(i,1),p25(i,1)],[p24(i,2),p25(i,2)],[p24(i,3),p25(i,3)],'color','m');
    plot3([p25(i,1),p26(i,1)],[p25(i,2),p26(i,2)],[p25(i,3),p26(i,3)],'color','m');
    plot3([p26(i,1),p27(i,1)],[p26(i,2),p27(i,2)],[p26(i,3),p27(i,3)],'color','m');
    
    plot3(p21(i,1),p21(i,2),p21(i,3),'o','color','m','MarkerSize',5);
    plot3(p22(i,1),p22(i,2),p22(i,3),'o','color','m','MarkerSize',5);
    plot3(p23(i,1),p23(i,2),p23(i,3),'o','color','m','MarkerSize',5);
    plot3(p24(i,1),p24(i,2),p24(i,3),'o','color','m','MarkerSize',5);
    plot3(p25(i,1),p25(i,2),p25(i,3),'o','color','m','MarkerSize',5);
    plot3(p26(i,1),p26(i,2),p26(i,3),'o','color','m','MarkerSize',5);
    plot3(p27(i,1),p27(i,2),p27(i,3),'o','color','m','MarkerSize',5);
    
     


% plot third leg
    ax43 = plot3([p31(i,1),p32(i,1)],[p31(i,2),p32(i,2)],[p31(i,3),p32(i,3)],'color','g');
 
    plot3([p32(i,1),p34(i,1)],[p32(i,2),p34(i,2)],[p32(i,3),p34(i,3)],'color','g');
    plot3([p32(i,1),p33(i,1)],[p32(i,2),p33(i,2)],[p32(i,3),p33(i,3)],'color','g');
    plot3([p33(i,1),p35(i,1)],[p33(i,2),p35(i,2)],[p33(i,3),p35(i,3)],'color','g');
    plot3([p34(i,1),p35(i,1)],[p34(i,2),p35(i,2)],[p34(i,3),p35(i,3)],'color','g');
    plot3([p35(i,1),p36(i,1)],[p35(i,2),p36(i,2)],[p35(i,3),p36(i,3)],'color','g');
    plot3([p36(i,1),p37(i,1)],[p36(i,2),p37(i,2)],[p36(i,3),p37(i,3)],'color','g');
    
    plot3(p31(i,1),p31(i,2),p31(i,3),'o','color','g','MarkerSize',5);
    plot3(p32(i,1),p32(i,2),p32(i,3),'o','color','g','MarkerSize',5);
    plot3(p33(i,1),p33(i,2),p33(i,3),'o','color','g','MarkerSize',5);
    plot3(p34(i,1),p34(i,2),p34(i,3),'o','color','g','MarkerSize',5);
    plot3(p35(i,1),p35(i,2),p35(i,3),'o','color','g','MarkerSize',5);
    plot3(p36(i,1),p36(i,2),p36(i,3),'o','color','g','MarkerSize',5);
    plot3(p37(i,1),p37(i,2),p37(i,3),'o','color','g','MarkerSize',5);
    
 

% plot fourth leg
    ax44 = plot3([p41(i,1),p42(i,1)],[p41(i,2),p42(i,2)],[p41(i,3),p42(i,3)],'color','c');

    plot3([p42(i,1),p44(i,1)],[p42(i,2),p44(i,2)],[p42(i,3),p44(i,3)],'color','c');
    plot3([p42(i,1),p43(i,1)],[p42(i,2),p43(i,2)],[p42(i,3),p43(i,3)],'color','c');
    plot3([p43(i,1),p45(i,1)],[p43(i,2),p45(i,2)],[p43(i,3),p45(i,3)],'color','c');
    plot3([p44(i,1),p45(i,1)],[p44(i,2),p45(i,2)],[p44(i,3),p45(i,3)],'color','c');
    plot3([p45(i,1),p46(i,1)],[p45(i,2),p46(i,2)],[p45(i,3),p46(i,3)],'color','c');
    plot3([p46(i,1),p47(i,1)],[p46(i,2),p47(i,2)],[p46(i,3),p47(i,3)],'color','c');
    
    plot3(p41(i,1),p41(i,2),p41(i,3),'o','color','c','MarkerSize',5);
    plot3(p42(i,1),p42(i,2),p42(i,3),'o','color','c','MarkerSize',5);
    plot3(p43(i,1),p43(i,2),p43(i,3),'o','color','c','MarkerSize',5);
    plot3(p44(i,1),p44(i,2),p44(i,3),'o','color','c','MarkerSize',5);
    plot3(p45(i,1),p45(i,2),p45(i,3),'o','color','c','MarkerSize',5);
    plot3(p46(i,1),p46(i,2),p46(i,3),'o','color','c','MarkerSize',5);
    plot3(p47(i,1),p47(i,2),p47(i,3),'o','color','c','MarkerSize',5);
    
    
% % plot spline2
ps = plot3([p17(i,1),p27(i,1)],[p17(i,2),p27(i,2)],[p17(i,3),p27(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p47(i,1),p37(i,1)],[p47(i,2),p37(i,2)],[p47(i,3),p37(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p127(i,1),p347(i,1)],[p127(i,2),p347(i,2)],[p127(i,3),p347(i,3)],'color',[0 0.4470 0.7410]);
    belly = plot3([pmdl(i,1),pbtm(i,1)],[pmdl(i,2),pbtm(i,2)],[pmdl(i,3),pbtm(i,3)],'color',[0 0.4470 0.7410]);
    
    hold off
view(ax40,[0,10,0])
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.45 0.45]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
title(ax40,'Front View');
legend('boxoff');
legend([ax4_traj, ax4_follow,ax41,ax42,ax43,ax44,ps,belly],'trajectory',['following-trajectory' newline 'points on the robot'],'1st leg','2nd leg','3rd leg','4th leg','spine','belly');

box on


% set the window size
set(gcf,'position',[100,60,1300,870]);

% pause(0.2);
       frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    

end

%%
% plot the joint trajectories
figure;
y = x(:,1:12).*180/pi;
ax_q = plot(y);
% plot(x(:,1));
lgd = legend(ax_q,'q11','q12','q13','q21','q22','q23','q31','q32','q33','q41','q42','q43');
lgd.NumColumns = 4;
title('Joint Trajectories for 4 Legs');
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
xlim([-1 2*n+2]);
ylim([-100 145]);

figure;
% plot joint trajectories with separated subplot

subplot(2,2,1);
ax_trjact1 = plot(x(:,(1:3)).*180/pi);
legend('q11','q12','q13');
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 1');
legend('boxoff');

subplot(2,2,2);
ax_trjact2 = plot(x(:,(4:6)).*180/pi);
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 2');
legend('q21','q22','q23');
legend('boxoff');

subplot(2,2,3);
ax_trjact3 = plot(x(:,(7:9)).*180/pi);
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 3');
legend('q31','q32','q33');
legend('boxoff');

subplot(2,2,4);
ax_trjact4 = plot(x(:,(10:12)).*180/pi);
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 4');
legend('q41','q42','q43');
legend('boxoff');

%% plot hip trajectories
figure;
hip_traj1 = plot(x(:,3).*180/pi);
hold on
hip_traj2 = plot(x(:,6).*180/pi);
hip_traj3 = plot(x(:,9).*180/pi);
hip_traj4 = plot(x(:,12).*180/pi);
hold off
xlim([-1 2*n+2]);
ylim([-100 100]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Hip Trajectories');
legend([hip_traj1,hip_traj2,hip_traj3,hip_traj4],'q13','q23','q33','q43');
legend('boxoff');

%%plot hip trajectories\
figure;
subplot(1,2,1);
hip_traj1 = plot(x(:,3).*180/pi);
hold on
hip_traj2 = plot(x(:,6).*180/pi);
hold off
xlim([-1 2*n+2]);
ylim([-100 100]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title(['Hip Trajectories in' newline 'First and Second Leg']);
legend([hip_traj1,hip_traj2],'q13','q23');
legend('boxoff');

subplot(1,2,2);
hip_traj3 = plot(x(:,9).*180/pi);
hold on
hip_traj4 = plot(x(:,12).*180/pi);
hold off
xlim([-1 2*n+2]);
ylim([-100 100]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title(['Hip Trajectories in' newline 'Third and Fourth Leg']);
legend([hip_traj3,hip_traj4],'q33','q43');
legend('boxoff');




