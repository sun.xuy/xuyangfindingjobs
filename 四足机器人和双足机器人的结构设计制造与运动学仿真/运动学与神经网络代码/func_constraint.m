% symbolically create constraints

syms l1 l2 l3 l4 l5 llth lwth lbb lbrk



x = sym('x%d',[1 12]);



% rotation around z
Z = @(z,q)   [1       0          0       0;...
              0   cos(q)    -sin(q)      0;...
              0   sin(q)     cos(q)      z;...
              0       0          0       1;] ;

% rotation around y
Y = @(q,x,y)     [   cos(q)     0     sin(q)  x;...
                         0      1         0   y;...
                    -sin(q)     0     cos(q)  0;...
                         0      0         0   1;];
% translation matrix

T = @(x,y)   [  1   1   1   x;...
                1   1   1   y;...
                1   1   1   0;...
                0   0   0   1;];




%%
% generate constraint from leg 1 to leg 2 
% transformation matrix from origin to zero point of the first leg
To10 = T(lwth/2,llth/2);
% transformation matrix from foot1 to hip drive

T100_pr = Y(0,0,x(3));
T10_pr1 = Z(0,pi/2-x(2));
T112 = Z(l1, -(pi-x(1)));
T123 = Z(l3,pi-x(1));
T135 = Z(l2,0);
T156 = Z(l4,-(pi/2-x(2)));
T167 = Y(0,-l5,-x(3));

% transformation matrix from hip in leg1 to hip in leg 2
T1727 = T(-lbrk,0);
% tansformation matrix from hip to feet in leg2
T276_pr = Y(0,l5,x(6));
T26_pr6 = Z(0,pi/2-x(5));
T265 = Z(-l4,0);
T253 = Z(-l2,-(pi-x(4)));
T232 = Z(-l3,pi-x(4));
T221 = Z(-l1,-(pi/2-x(5)));
T210 = Y(0,0,-x(6));

% tansformation matrix from foot in leg 2 to foot in leg 1
T2010 = T(lwth,0);

% set foot 1 a starting point, go through leg 2 and goes back to foot 1 again
% create a constraint for leg 1 and 2
temp = T100_pr*T10_pr1*T112*T123*T135*T156*T167*T1727*T276_pr*T26_pr6*T265*T253*T232*T221*T210*T2010;
c12 = temp(1:3,4);






% generate constraint from leg 3 to leg 4 
% transformation matrix from origin to zero point of the first leg
To30 = T(lwth/2,-llth/2);
% transformation matrix from foot1 to hip drive

T300_pr = Y(0,0,x(9));
T30_pr1 = Z(0,pi/2-x(8));
T312 = Z(l1, -(pi-x(7)));
T323 = Z(l3,pi-x(7));
T335 = Z(l2,0);
T356 = Z(l4,-(pi/2-x(8)));
T367 = Y(0,-l5,-x(9));

% transformation matrix from hip in leg1 to hip in leg 2
T3747 = T(-lbrk,0);
% tansformation matrix from hip to feet in leg2
T476_pr = Y(0,l5,x(12));
T46_pr6 = Z(0,pi/2-x(11));
T465 = Z(-l4,0);
T453 = Z(-l2,-(pi-x(10)));
T432 = Z(-l3,pi-x(10));
T421 = Z(-l1,-(pi/2-x(11)));
T410 = Y(0,0,-x(12));

% tansformation matrix from foot in leg 4 to foot in leg 2
T4030 = T(lwth,0);

% set foot 3 a starting point, go through leg 4 and goes back to foot 3 again
% create a constraint for leg 3 and 4
temp = T300_pr*T30_pr1*T312*T323*T335*T356*T367*T3747*T476_pr*T46_pr6*T465*T453*T432*T421*T410*T4030;
c34 = temp(1:3,4);

%%

% generate constraint between leg 1 and leg 3
% transformation matrix from foot1 to hip drive

T100_pr = Y(0,0,x(3));
T10_pr1 = Z(0,pi/2-x(2));
T112 = Z(l1, -(pi-x(1)));
T123 = Z(l3,pi-x(1));
T135 = Z(l2,0);
T156 = Z(l4,-(pi/2-x(2)));
T167 = Y(0,-l5,-x(3));
          
% transformation matrix from hip 1 to hip3

T1737 = T(0,-lbb);

% transformation matrix from hip to feet in leg 3
T376_pr = Y(0,l5,x(9));
T36_pr6 = Z(0,pi/2-x(8));
T365 = Z(-l4,0);
T353 = Z(-l2,-(pi-x(7)));
T332 = Z(-l3,pi-x(7));
T321 = Z(-l1,-(pi/2-x(8)));
T310 = Y(0,0,-x(9));

% tansformation matrix from foot in leg 3 to foot in leg 1
T3010 = T(0,llth);

% set foot 1 a starting point, go through leg 3 and goes back to foot 1 again
% create a constraint for leg 1 and 3

temp = T100_pr*T10_pr1*T112*T123*T135*T156*T167*T1737*T376_pr*T36_pr6*T365*T353*T332*T321*T310*T3010;

c13 = temp(1:3,4); 



% generate constraint between leg 2 and leg 4
% transformation matrix from foot1 to hip drive

T200_pr = Y(0,0,x(6));
T20_pr1 = Z(0,pi/2-x(5));
T212 = Z(l1, -(pi-x(4)));
T223 = Z(l3,pi-x(4));
T235 = Z(l2,0);
T256 = Z(l4,-(pi/2-x(5)));
T267 = Y(0,-l5,-x(6));
          
% transformation matrix from hip 2 to hip4

T2747 = T(0,-lbb);

% transformation matrix from hip to feet in leg 4
T476_pr = Y(0,l5,x(12));
T46_pr6 = Z(0,pi/2-x(11));
T465 = Z(-l4,0);
T453 = Z(-l2,-(pi-x(10)));
T432 = Z(-l3,pi-x(10));
T421 = Z(-l1,-(pi/2-x(11)));
T410 = Y(0,0,-x(12));

% tansformation matrix from foot in leg 4 to foot in leg 2
T4020 = T(0,llth);

% set foot 2 a starting point, go through leg 4 and goes back to foot 1 again
% create a constraint for leg 2 and 4

temp = T200_pr*T20_pr1*T212*T223*T235*T256*T267*T2747*T476_pr*T46_pr6*T465*T453*T432*T421*T410*T4020;

c24 = temp(1:3,4); 





%%
% generate constraint between leg 1 and leg 4
% transformation matrix from foot1 to hip drive

T100_pr = Y(0,0,x(3));
T10_pr1 = Z(0,pi/2-x(2));
T112 = Z(l1, -(pi-x(1)));
T123 = Z(l3,pi-x(1));
T135 = Z(l2,0);
T156 = Z(l4,-(pi/2-x(2)));
T167 = Y(0,-l5,-x(3));
          
% transformation matrix from hip 1 to hip4

T1747 = T(-lbrk,-lbb);


% transformation matrix from hip to feet in leg 3
T476_pr = Y(0,l5,x(12));
T46_pr6 = Z(0,pi/2-x(11));
T465 = Z(-l4,0);
T453 = Z(-l2,-(pi-x(10)));
T432 = Z(-l3,pi-x(10));
T421 = Z(-l1,-(pi/2-x(11)));
T410 = Y(0,0,-x(12));

% tansformation matrix from foot in leg 4 to foot in leg 1
T4010 = T(llth,lwth);

temp = T100_pr*T10_pr1*T112*T123*T135*T156*T167*T1747*T476_pr*T46_pr6*T465*T453*T432*T421*T410*T4010;
c14 = temp(1:3,4); 



% generate constraint between leg 2 and leg 3
% transformation matrix from foot1 to hip drive

T200_pr = Y(0,0,x(6));
T20_pr1 = Z(0,pi/2-x(5));
T212 = Z(l1, -(pi-x(4)));
T223 = Z(l3,pi-x(4));
T235 = Z(l2,0);
T256 = Z(l4,-(pi/2-x(5)));
T267 = Y(0,-l5,-x(6));
          
          
% transformation matrix from hip 2 to hip3

T2737 = T(lbrk,-lbb);


% transformation matrix from hip to feet in leg 3

T376_pr = Y(0,l5,x(9));
T36_pr6 = Z(0,pi/2-x(8));
T365 = Z(-l4,0);
T353 = Z(-l2,-(pi-x(7)));
T332 = Z(-l3,pi-x(7));
T321 = Z(-l1,-(pi/2-x(8)));
T310 = Y(0,0,-x(9));


% tansformation matrix from foot in leg 2 to foot in leg 3
T3020 = T(-llth,lwth);

temp = T200_pr*T20_pr1*T212*T223*T235*T256*T267*T2737*T376_pr*T36_pr6*T365*T353*T332*T321*T310*T3010;
c23 = temp(1:3,4); 

% transformation matrix from origin to pb1
Tpb1 = T(-lwth/2,-llth/2);
pb1 =  To10*T100_pr*T10_pr1*T112*T123*T135*T156*T167*Tpb1;

% transformation matrix from origin to pb2
Tpb2 = T(-lwth/2+0.01,-llth/2);
pb2 =  To10*T100_pr*T10_pr1*T112*T123*T135*T156*T167*Tpb2;

% transformation matrix from origin to pb3
Tpb3 = T(-lwth/2,-llth/2+0.01);
pb3 =  To10*T100_pr*T10_pr1*T112*T123*T135*T156*T167*Tpb3;

