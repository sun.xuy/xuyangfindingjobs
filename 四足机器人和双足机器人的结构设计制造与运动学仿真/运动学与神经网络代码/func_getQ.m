function x = func_getQ(x0,pb1_traj)

options = optimset('Display','off');
A = [];
b = [];
Aeq = [];
beq = [];
% lb = [80 20 -89 80 20 -89 80 20 -89 80 20 -89 0.55*180/pi 0.15*180/pi]*pi/180;
% ub = [160 89 89 160 89 89 160 89 89 160 89 89 0.55*180/pi 0.15*180/pi]*pi/180;
lb = [];
ub = [];


% run fmincon function to calculate the optimal solution
[x,J] = fmincon(@func_cost,x0,[],[],[],[],[],[],@loopconstraint,options);

% define the cost function
    function C = func_cost(x)
        
        [c12,c13,c14,c23,c24,c34,p11,p12,p13,p14,p15,p16,p17,p21,p22,p23,p24,p25,p26,p27,p31,p32,p33,p34,p35,p36,p37,p41,p42,p43,p44,p45,p46,p47,pb1,pb2,pb3] = func_fwd_kinematics(x);
             Y = pb1 - pb1_traj.';
%              Y = [pb1-pb1_traj.';pb2-pb2_traj.'];
%              Y = [pb1-pb1_traj.';pb2-pb2_traj.';pb3-pb3_traj.'];
        C = Y.'*Y;
        
    end

% define the constraints
    function [c,ceq] = loopconstraint(x)
       [c12,c13,c14,c23,c24,c34,p11,p12,p13,p14,p15,p16,p17,p21,p22,p23,p24,p25,p26,p27,p31,p32,p33,p34,p35,p36,p37,p41,p42,p43,p44,p45,p46,p47,pb1,pb2,pb3] = func_fwd_kinematics(x);
%           d1 = [p16(3)];
%           d2 = [p26(3)];
%           d3 = [p36(3)];
%           d4 = [p46(3)];
 %              c = [-d1;-d2;-d3;-d4];
            c= [];

          ceq = [c12;c13;c14];
          
%         c = abs([c12;c13;c14;c23;c24;c34]) - 0.00000001;
%           ceq = [x(3);x(6);x(9);x(12)];
%         
    end
end


