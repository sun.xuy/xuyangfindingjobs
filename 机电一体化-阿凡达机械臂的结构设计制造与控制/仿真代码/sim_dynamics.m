% simulate the dynamics of the system
% 12/03/2019 Writen by Xuyang Sun, Boston, MA



q0 = [0, 0, 0, 0, 0].'*pi/180;
dq0 = [0, 0, 0, 0, 0].';

% Get desired trajectory (as a step command) from inverse kinematics)
q_d = 10*[1, 2, 3, 4, 5].'*pi/180;
dq_d = [0, 0, 0, 0, 0].';

x0 = [q0; dq0];

options = odeset('RelTol',1e-2,'AbsTol',1e-4);

tf = 0.7;

[t,x_sol] = ode45(@(t,x) func_dx(t,x, q_d, dq_d), [0 tf], x0, options);


%%
figure;

sgtitle('Motor Angle Trajectory of the Avatar Arm');

subplot(5,1,1)
plot(t,x_sol(:,1)*180/pi)
ylabel('q1')
xlabel('time(s)')


subplot(5,1,2)
plot(t,x_sol(:,2)*180/pi)
ylabel('q2')
xlabel('time')

subplot(5,1,3)
plot(t,x_sol(:,3)*180/pi)
ylabel('q3')
xlabel('time')

subplot(5,1,4)
plot(t,x_sol(:,4)*180/pi)
ylabel('q4')
xlabel('time')

subplot(5,1,5)
plot(t,x_sol(:,5)*180/pi)
ylabel('q5')
xlabel('time')

figure;
sgtitle('Motor Angular Velocity of the Avatar Arm');
subplot(5,1,1)
plot(t,x_sol(:,6))
ylabel('dq1')
xlabel('time')

subplot(5,1,2)
plot(t,x_sol(:,7))
ylabel('dq2')
xlabel('time')

subplot(5,1,3)
plot(t,x_sol(:,8))
ylabel('dq3')
xlabel('time')

subplot(5,1,4)
plot(t,x_sol(:,9))
ylabel('dq4')
xlabel('time')

subplot(5,1,5)
plot(t,x_sol(:,10))
ylabel('dq5')
xlabel('time')







%%


figure;
filename = 'sim_dynamics.gif';
skip_data = 100;

for i = 1:skip_data:length(t)
    
    
    % forward kinematics
    q1 = x_sol(i,1);
    q2 = x_sol(i,2);
    q3 = x_sol(i,3);
    q4 = x_sol(i,4);
    q5 = x_sol(i,5);
    
    [p1,p2,p3,p4,p5] = func_fwdK(q1,q2,q3,q4,q5);
    
    %     plot the trajectories
%     axTraj = plot3(x,y,z,'--','linewidth',0.5);
    
    %     plot the platform
    %
    %     plot3([0,0.5],[0,0],[0,0]);
    axPlatform = plot3([0.5,0.5],[0,1],[0,0],'Linewidth',2,'Color','k'); %1
    hold on
    plot3([0.5,0.5],[0,0],[0,1],'Linewidth',2,'Color','k'); %12
    plot3([0.5,0.5],[0,1],[1,1],'Linewidth',2,'Color','k'); %5
    plot3([0.5,0.5],[1,1],[0,1],'Linewidth',2,'Color','k'); %9
    
    
    plot3([0.5,-0.5],[1,1],[0,0],'Linewidth',2,'Color','k');%2
    plot3([0.5,-0.5],[1,1],[1,1],'Linewidth',2,'Color','k');%6
    plot3([0.5,-0.5],[0,0],[0,0],'Linewidth',2,'Color','k');%4
    plot3([0.5,-0.5],[0,0],[1,1],'Linewidth',2,'Color','k');%8
    
    plot3([-0.5,-0.5],[0,0],[0,1],'Linewidth',2,'Color','k'); %11
    plot3([-0.5,-0.5],[1,1],[0,1],'Linewidth',2,'Color','k'); %10
    plot3([-0.5,-0.5],[0,1],[1,1],'Linewidth',2,'Color','k'); %7
    plot3([-0.5,-0.5],[0,1],[0,0],'Linewidth',2,'Color','k'); %3
    %
    % plot 3d of the robotic arm
    
    
    
    %     axOrigin = plot3([0,0],[0,0],[0,0],'h');
    
    %     plot3([xOffset,xOffset],[yOffset,yOffset],[zOffset,zOffset],'h');
    axLink1 = plot3([p1( 1),p2( 1)],[p1( 2),p2( 2)],[p1( 3),p2( 3)],'color',[0.5 0.4470 0.7410],'Linewidth',3);
    
    plot3([p2( 1),p3( 1)],[p2( 2),p3( 2)],[p2( 3),p3( 3)],'color',[0 0.4470 0.7410],'Linewidth',3);
    plot3([p3( 1),p4( 1)],[p3( 2),p4( 2)],[p3( 3),p4( 3)],'color',[0 0.4470 0.7410],'Linewidth',3);
    axLink2 =  plot3([p4( 1),p5( 1)],[p4( 2),p5( 2)],[p4( 3),p5( 3)],'color',[0.8500 0.3250 0.0980],'Linewidth',3);
    
    axBase = plot3([p1( 1),p1( 1)],[p1( 2),p1( 2)],[p1( 3),p1( 3)],'o','MarkerSize',10,'MarkerFaceColor','r');
    axDifferential = plot3([p2( 1),p2( 1)],[p2( 2),p2( 2)],[p2( 3),p2( 3)],'d','MarkerSize',7,'MarkerFaceColor','b');
    %     plot3([p3( 1),p3( 1)],[p3( 2),p3( 2)],[p3( 3),p3( 3)],'o');
    plot3([p4( 1),p4( 1)],[p4( 2),p4( 2)],[p4( 3),p4( 3)],'d','MarkerSize',7,'MarkerFaceColor','b');
    axEndEffector= plot3([p5( 1),p5( 1)],[p5( 2),p5( 2)],[p5( 3),p5( 3)],'o','MarkerSize',5);
    
    hold off
    view([10,15,10]);
    
    axis equal
    xlabel('x');
    ylabel('y');
    zlabel('z');
    title('Avater Arm Simulation');
    ylim([0 2.5]);
    xlim([-0.5 0.5]);

    zlim([0 1.5]);
    box on
    
    legend([axPlatform,axBase,axLink1,axLink2,axDifferential,axEndEffector],'Platform','Base','Arm Link1','Arm Link2','Differential','EndEffector');
    legend boxoff
    
    
    
    
    % set the window size
    set(gcf,'position',[100,60,1300,870]);
    
    % pause(0.2);
    frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    
    
end








