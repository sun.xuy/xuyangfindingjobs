% simulation of the robotic arm for ME5245, Mechatronic System
% 12/02/2019 Writen by Xuyang Sun, Boston, MA
close all;
clear all;
clc;




%%
n = 50;

% % define fwd trajectory

% q1 = pi/180*linspace(5,10,n);
% q2 = pi/180*linspace(-30,-10,n);
% q3 = pi/180*linspace(-30,-10,n);
% q4 = pi/180*linspace(-30,-10,n);
% q5 = pi/180*linspace(-30,-10,n);
% 

% % run fwdK
% for i = 1:n;
%     
% [p1(i,:),p2(i,:),p3(i,:),p4(i,:),p5(i,:)] = func_fwdK(q1(i),q2(i),q3(i),q4(i),q5(i));
% 
% end
% 
% 


%%
params;

% set the trajectory
x = linspace(-0.2,0.2,n);
y = linspace(1.6,1.8,n);
z = 0.2*sin(15*x)+0.5;

% x = linspace(-0.2,0.2,n);
% y = linspace(1.5,1.9,n);
% z = linspace(0.5,0.9,n);

traj=[x;y;z];

% set initial guess in radians
q0 = [8,3,-1,30,30]*pi/180;

options = optimset('Display','off');

% using fsolve function to calculate the joint trajectories for each sample
% trajectory (inverse kinematics)

for i=1:n

F = @(x) func_getQ(x,traj(:,i));
q = fsolve(F,q0,options);
Q(:,i) = q;

end

q1 = Q(1,:);
q2 = Q(2,:);
q3 = Q(3,:);
q4 = Q(4,:);
q5 = Q(5,:);

% based on the joint trajectories generate by inverse kinematics, plug in
% all the joint trajectories and using forward kinematics to calculate the
% position of each joint.

for i = 1:n
% fwdK
[p1(i,:),p2(i,:),p3(i,:),p4(i,:),p5(i,:)] = func_fwdK(q1(i),q2(i),q3(i),q4(i),q5(i));

end 


%%

% plot the robot

filename = 'sim_mechanism_.gif';
for i = 1:n
    
%     plot the trajectories
    axTraj = plot3(x,y,z,'--','linewidth',0.5); 
    hold on
%     plot the platform
% 
%     plot3([0,0.5],[0,0],[0,0]);
    axPlatform = plot3([0.5,0.5],[0,1],[0,0],'Linewidth',2,'Color','k'); %1
    plot3([0.5,0.5],[0,0],[0,1],'Linewidth',2,'Color','k'); %12
    plot3([0.5,0.5],[0,1],[1,1],'Linewidth',2,'Color','k'); %5
    plot3([0.5,0.5],[1,1],[0,1],'Linewidth',2,'Color','k'); %9
    
    
    plot3([0.5,-0.5],[1,1],[0,0],'Linewidth',2,'Color','k');%2
    plot3([0.5,-0.5],[1,1],[1,1],'Linewidth',2,'Color','k');%6
    plot3([0.5,-0.5],[0,0],[0,0],'Linewidth',2,'Color','k');%4
    plot3([0.5,-0.5],[0,0],[1,1],'Linewidth',2,'Color','k');%8
    
    plot3([-0.5,-0.5],[0,0],[0,1],'Linewidth',2,'Color','k'); %11
    plot3([-0.5,-0.5],[1,1],[0,1],'Linewidth',2,'Color','k'); %10   
    plot3([-0.5,-0.5],[0,1],[1,1],'Linewidth',2,'Color','k'); %7
    plot3([-0.5,-0.5],[0,1],[0,0],'Linewidth',2,'Color','k'); %3
%    
% plot 3d of the robotic arm


    
%     axOrigin = plot3([0,0],[0,0],[0,0],'h');
 
%     plot3([xOffset,xOffset],[yOffset,yOffset],[zOffset,zOffset],'h');
     axLink1 = plot3([p1(i,1),p2(i,1)],[p1(i,2),p2(i,2)],[p1(i,3),p2(i,3)],'color',[0.5 0.4470 0.7410],'Linewidth',3);

     plot3([p2(i,1),p3(i,1)],[p2(i,2),p3(i,2)],[p2(i,3),p3(i,3)],'color',[0 0.4470 0.7410],'Linewidth',3);
     plot3([p3(i,1),p4(i,1)],[p3(i,2),p4(i,2)],[p3(i,3),p4(i,3)],'color',[0 0.4470 0.7410],'Linewidth',3);
     axLink2 =  plot3([p4(i,1),p5(i,1)],[p4(i,2),p5(i,2)],[p4(i,3),p5(i,3)],'color',[0.8500 0.3250 0.0980],'Linewidth',3);

    axBase = plot3([p1(i,1),p1(i,1)],[p1(i,2),p1(i,2)],[p1(i,3),p1(i,3)],'o','MarkerSize',10,'MarkerFaceColor','r');
    axDifferential = plot3([p2(i,1),p2(i,1)],[p2(i,2),p2(i,2)],[p2(i,3),p2(i,3)],'d','MarkerSize',7,'MarkerFaceColor','b');
%     plot3([p3(i,1),p3(i,1)],[p3(i,2),p3(i,2)],[p3(i,3),p3(i,3)],'o');
    plot3([p4(i,1),p4(i,1)],[p4(i,2),p4(i,2)],[p4(i,3),p4(i,3)],'d','MarkerSize',7,'MarkerFaceColor','b');
    axEndEffector= plot3([p5(i,1),p5(i,1)],[p5(i,2),p5(i,2)],[p5(i,3),p5(i,3)],'o','MarkerSize',5);
    
    hold off
    view([10,15,10]);
    
xlabel('x');
ylabel('y');
zlabel('z');
title('Avater Arm Simulation');
% xlim([-0.5 1.5]);
ylim([0 2]);
% zlim([0 2]);
box on

legend([axTraj,axPlatform,axBase,axLink1,axLink2,axDifferential,axEndEffector],'Trajectory','Platform','Base','Arm Link1','Arm Link2','Differential','EndEffector');
legend boxoff




% set the window size
set(gcf,'position',[100,60,1300,870]);

% pause(0.2);
       frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    

end




%%

figure
plot(q1)
hold on
plot(q2)
plot(q3)
plot(q4)
plot(q5)
hold off
legend('q_1','q_2','q_3','q_4','q_5');
title('Motor Angle Trajctory');
xlabel('n[time step]');
ylabel('angle[rads]')


%%
error = traj-p5';

figure;
plot(error(1,:));
hold on
plot(error(2,:));
plot(error(3,:));
hold off

legend('error in x axis','error in y axis','error in z axis' );
title('error between actual and desired end effector position');
xlabel('n[time step]');
ylabel('length[meters]')




































