
clear all;

% import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames')
calib_gps = readtable('three_loops_gps_data.csv');
calib_imu = readtable('three_loops_imu_data.csv');

% get GPS easting and northing
easting = calib_gps.field_utmeasting;
northing = calib_gps.field_utmnorthing;
% calculate velocity from GPS
vel_gps = sqrt(easting.^2+northing.^2);
vel_gps_cumsum = cumsum(vel_gps);


% get acceleration in x direction, 40 is frequency
calib_accel_x = calib_imu.field_linear_acceleration_x;

vel_imu_cumsum = 1/40*cumtrapz(calib_accel_x);

figure;
plot(vel_gps)
hold on
plot(vel_imu_cumsum)
hold off









