
clear all;
close all;


% import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames')
imu = load('stay_1.mat');


imu_accel_x = imu.robot.imu_data.acceleration_x;
imu_accel_y = imu.robot.imu_data.acceleration_y;
imu_accel_z = imu.robot.imu_data.acceleration_z;

imu_accel_x_offset = mean(imu_accel_x); % -0.1069 m/s^2
imu_accel_y_offset = mean(imu_accel_y); % -0.2200 m/s^2
imu_accel_z_offset = mean(imu_accel_z); % -9.7627 m/s^2

error_accel_x = imu_accel_x - mean(imu_accel_x);
error_accel_y = imu_accel_y - mean(imu_accel_y);
error_accel_z = imu_accel_z - mean(imu_accel_z);


imu_angular_velocity_x = imu.robot.imu_data.angular_velocity_x;
imu_angular_velocity_y = imu.robot.imu_data.angular_velocity_y;
imu_angular_velocity_z = imu.robot.imu_data.angular_velocity_z;

% imu_angular_velocity_x_offset = mean(imu_angular_velocity_x);
% imu_angular_velocity_y_offset = mean(imu_angular_velocity_y);
% imu_angular_velocity_z_offset = mean(imu_angular_velocity_z);


imu_Magx = imu.robot.mag_data.magnetic_field_x;
imu_Magy = imu.robot.mag_data.magnetic_field_y;
imu_Magz = imu.robot.mag_data.magnetic_field_z;

imu_Magx_offset = mean(imu_Magx);
imu_Magy_offset = mean(imu_Magy);
imu_Magz_offset = mean(imu_Magz);

error_imu_Magx_offset = imu_Magx - imu_Magx_offset;
error_imu_Magy_offset = imu_Magy - imu_Magy_offset;
error_imu_Magz_offset = imu_Magz - imu_Magz_offset;



%%
% acceleration data analysis
n = 1:1:1800;
y1 = 0*n+imu_accel_x_offset;
y2 = 0*n+imu_accel_y_offset;
y3 = 0*n+imu_accel_z_offset;



figure();
subplot(3,2,1)
plot(imu_accel_x)
hold on 
plot(n,y1,'r')
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('accleration[m/s^2]')
legend('raw data','offset')
title('acceleration in x direction')


subplot(3,2,3)
plot(imu_accel_y)
hold on 
plot(n,y2,'r')
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('accleration[m/s^2]')
legend('raw data','offset')
title('acceleration in y direction')

subplot(3,2,5)
plot(imu_accel_z)
hold on 
plot(n,y3,'r')
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('accleration[m/s^2]')
legend('raw data','offset')
title('acceleration in z direction')

% plot of accelerator

% y_accel_x = y_accel_x';

y_accel_x = 0*n;

subplot(3,2,2)
plot(error_accel_x);
hold on 
plot(n,y_accel_x,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('accleration[m/s^2]')
legend('noise','group truth')
title('noise acceleration in x direction')


subplot(3,2,4)
plot(error_accel_y)
hold on 
plot(n,y_accel_x,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('accleration[m/s^2]')
legend('noise','group truth')
title('noise acceleration in y direction')

subplot(3,2,6)
plot(error_accel_z);
hold on 
plot(n,y_accel_x,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('accleration[m/s^2]')
legend('noise','group truth')
title('noise acceleration in z direction')

sgtitle('data analysis in acceleration')

%%
% angular velocity analysis

figure();

subplot(3,1,1)
plot(imu_angular_velocity_x)
hold on 
plot(n,y_accel_x,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('angular velocity [rad/s]')
legend('noise','group truth')
title('angular velocity around x')

subplot(3,1,2)
plot(imu_angular_velocity_y)
hold on 
plot(n,y_accel_x,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('angular velocity [rad/s]')
legend('noise','group truth')
title('angular velocity around y')

subplot(3,1,3)
plot(imu_angular_velocity_z)
hold on 
plot(n,y_accel_x,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('angular velocity [rad/s]')
legend('noise','group truth')
title('angular velocity around z')

sgtitle('data analysis in angular velocity')

%%
% magnetometer
y11 = 0*n+imu_Magx_offset;
y12 = 0*n+imu_Magy_offset;
y13 = 0*n+imu_Magz_offset;


figure();
subplot(3,2,1)
plot(imu_Magx)
hold on 
plot(n,y11,'r')
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('magnetometer[Guass]')
legend('raw data','offset')
title('magnetometer in x direction')


subplot(3,2,3)
plot(imu_Magy)
hold on 
plot(n,y12,'r')
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('magnetometer[Guass]')
legend('raw data','offset')
title('magnetometer in y direction')

subplot(3,2,5)
plot(imu_Magz)
hold on 
plot(n,y13,'r')
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('magnetometer[Guass]')
legend('raw data','offset')
title('magnetometer in z direction')

origin = 0*n;


subplot(3,2,2)
plot(error_imu_Magx_offset);
hold on 
plot(n,origin,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('magnetometer[Guass]')
legend('noise','group truth')
title('noise magnetometer in x direction')


subplot(3,2,4)
plot(error_imu_Magy_offset)
hold on 
plot(n,origin,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('magnetometer[Guass]')
legend('noise','group truth')
title('noise magnetometer in y direction')

subplot(3,2,6)
plot(error_imu_Magz_offset);
hold on 
plot(n,origin,'r');
hold off
xlim([1,1800]);
xlabel('time step[n]')
ylabel('magnetometer[Guass]')
legend('noise','group truth')
title('noise magnetometer in z direction')

sgtitle('data analysis in magnetometer')




