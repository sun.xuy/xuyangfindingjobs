
close all;
clear all;
clc;

%import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames');
loop_imu = readtable('big_loop_imu_data.csv');
loop_YPR = readtable('big_loop_YPR_data.csv');
loop_gps = readtable('big_loop_gps_data.csv');
loop_Mag = readtable('big_loop_Mag_data.csv');


% still = 365;
% 
% accel_x = loop_imu.field_linear_acceleration_x;
% % accel_x_offset = mean(accel_x(1:still));
% 
% % plot(accel_x)
% accel_y = loop_imu.field_linear_acceleration_y;
% accel_y_offset = mean(accel_y(1:still));
% % plot(accel_y)


% 
% plot gps map
utm_easting = loop_gps.field_utmeasting;
utm_northing = loop_gps.field_utmnorthing;
% 
% % vel from gps
% easting_v = diff(utm_easting);
% easting_v1 = easting_v(1:896);
% northing_v = diff(utm_northing);
% northing_v1 = northing_v(1:896);
% vel_gps = sqrt(easting_v1.^2+northing_v1.^2);
% yaw_integration_time = yaw_integration(1:40:end);
% plot(yaw_integration_time)

% x_e = cumtrapz(easting_v1).*cos(yaw_integration_time)
% x_n = cumtrapz(northing_v1).*sin(yaw_integration_time)
% plot(x_e,x_n)
% 




starting_point = 3600;
% plot to know vehicle starts from sampling point 3600
% yaw_degree = loop_YPR.field_Yaw(starting_point:end);
% yaw = yaw_degree*pi/180;
% figure;
% plot(yaw);

% from calibration file 
offset_x = -0.0179;
offset_y = -0.1959;
sigma = 0.7919;
theta = -0.5113;
gamma1 = theta;
gamma2 = -theta;
R1 = [cos(gamma1) -sin(gamma1);sin(gamma1) cos(gamma1)];
R2 = [cos(gamma2) -sin(gamma2);sin(gamma2) cos(gamma2)];

% intercept data
Mag_x = loop_Mag.field_magnetic_field_x(starting_point:end);
Mag_y = loop_Mag.field_magnetic_field_y(starting_point:end);

% remove hard iron
Mag_x1 = Mag_x - offset_x;
Mag_y1 = Mag_y - offset_y;

% concatenate magnetometer
vec = [Mag_x1';Mag_y1'];
% remove soft iron
p_hr = R1*vec;
tmp = p_hr(1,:).*sigma;
p_hrs = [tmp;p_hr(2,:)];
p_hrsr = R2*p_hrs;
Mag_corrected = p_hrsr;

yaw_Mag_transpose = atan2(-Mag_corrected(2,:),Mag_corrected(1,:));
yaw_Mag = yaw_Mag_transpose';
yaw_Mag = wrapToPi(yaw_Mag);



z_angular_vel = loop_imu.field_angular_velocity_z+0.015;
z_angular_vel = z_angular_vel(starting_point:end);
yaw_integration = cumtrapz(0.025,z_angular_vel);
yaw_integration = wrapToPi(yaw_integration-1.5);

yaw_raw = atan2(-Mag_y,Mag_x);

a = 0.9;
yaw_complimentary = (1-a)*yaw_Mag+a*yaw_integration;

figure
plot(yaw_Mag)
hold on 
plot(yaw_raw)
plot(yaw_integration)
plot(yaw_complimentary)
legend('mag','raw','integration')


%%
% figure;
% yaw_Mag = atan2(-Mag_y,Mag_x);
% plot(yaw_Mag)

% loop_Mag = readtable('big_loop_Mag_data.csv');
% Mag_z = loop_Mag.field_magnetic_field_z;
% plot(Mag_z)

imu_accel_x = loop_imu.field_linear_acceleration_x(starting_point:end)-0.78;
% imu_accel_x = loop_imu.field_linear_acceleration_x(starting_point:end);
imu_accel_y = loop_imu.field_linear_acceleration_y(starting_point:end);
imu_angular_velocity_z = loop_imu.field_angular_velocity_z(starting_point:end);

% imu_accel_x_filtered = bandpass(imu_accel_x,[0.1 10],40);



vel_x = cumtrapz(0.025,imu_accel_x);
length = length(vel_x)
% manipulation of velocity from imu
fs = 40

% % 4.5-7.8
% % point from 0-213*fs
% tmp1 = linspace(4.5,7.8,213*fs);
% 
% for i = 1:213*fs
%    
%     vel_x(i) = vel_x(i)+tmp1(i);
%     
% end
% 
% % 7.5-0
% % point from 214*fs-286*fs
% tmp2 = linspace(7.7,7.3,(10000-8520));
% 
% % 8521-10000
% 
% 
% for i = (8521):10000
%    
%     vel_x(i) = vel_x(i)+tmp2(i-8520);
%     
% end
% 
% 
% % 245-268
% % 10000-10720
% 
% tmp3 = linspace(5,4.2,720);
% for i = 10001:10720
%    
%     vel_x(i) = vel_x(i)+tmp3(i-10000);
%     
% end
% 
% % 10721-287*fs = 760
% 
% tmp4 = linspace(4,0,760);
% for i = 10721:287*fs
%    
%     vel_x(i) = vel_x(i)+tmp4(i-10720);
%     
% end
% % 405-415
% tmp5 = linspace(2.3,2,400);
% for i = 16201:16600
%    
%     vel_x(i) = vel_x(i)+tmp4(i-16200);
%     
% end
% % 415-546
% tmp5 = linspace(1,1,5240);
% for i = 16601:21840
%    
%     vel_x(i) = vel_x(i)+tmp5(i-16600);
%     
% end
% % 546-615
% 
% tmp6 = linspace(1.5,2.6,2760);
% for i = 21841:24600
%    
%     vel_x(i) = vel_x(i)+tmp6(i-21840);
%     
% end
% 
% % 768-813
% tmp7 = linspace(-1.5,-0.9,2760);
% for i = 30721:32520
%    
%     vel_x(i) = vel_x(i)+tmp7(i-30720);
%     
% end
% % 707-714
% tmp7 = linspace(1.8,2,280);
% for i = 28281:28560
%    
%     vel_x(i) = vel_x(i)+tmp7(i-28280);
%     
% end
% % vel_x(165:247)+7.2*ones(83,1);



tmp = vel_x.*imu_angular_velocity_z;
% % 
figure;
subplot(2,1,1)
plot(imu_accel_x)
xlabel('time step[n]')
ylabel('acceleration[m/s^2]')
title('acceleration in x')


figure;
subplot(2,1,1)
plot(imu_accel_x)
xlabel('time step[n]')
ylabel('acceleration[m/s^2]')
title('acceleration in x')

subplot(2,1,2)
plot(imu_accel_y)
xlabel('time step[n]')
ylabel('acceleration[m/s^2]')
title('acceleration in y')




figure;
plot(imu_accel_y)
hold on 
plot(tmp)
hold off
legend('imu accel in y',"Wx'")
title("comparison of y'' and x'*w")


easting_v = diff(utm_easting);
northing_v = diff(utm_northing);
vel_gps_circle_included = sqrt(easting_v.^2+northing_v.^2);
vel_gps = vel_gps_circle_included(91:end);
figure
plot(vel_gps)
hold on 
plot(vel_x(1:40:end))
hold off
legend('vel gps','vel x')
xlabel('time[s]');
ylabel('velocity[m/s]')
title('forward velocity of the trip')
%%



%%

yaw_integration = yaw_integration-40*pi/180;
% tmp1 = diff(vel_x_cum);
% vel_x = [tmp1;tmp1(end)];
 vel_e = vel_x.*sin(yaw_integration);
vel_n = vel_x.*cos(yaw_integration);



x_e = cumtrapz(0.025,vel_e);
x_n = cumtrapz(0.025,vel_n);

figure;
subplot(1,2,2)
plot(x_e,x_n)
xlabel('easting[m]')
ylabel('northing[m]')
title('map from IMU')
axis equal

subplot(1,2,1)
plot(utm_easting,utm_northing);
title('map from GPS')
xlabel('easting[m]')
ylabel('northing[m]')
axis equal

%%





























