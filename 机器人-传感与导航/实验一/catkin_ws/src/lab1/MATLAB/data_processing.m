% clear all;
% close all;
% clc;
% 
% walking_data = readtable('walking1.csv');
% walking_data2 = readtable('walking2.csv');
% 
% one_place_data = readtable('one_place.csv');

walking_utm_easting = walking_data.field_utmeasting;
walking_utm_northing = walking_data.field_utmnorthing;

one_place_utm_easting = one_place_data.field_utmeasting;
one_place_utm_northing = one_place_data.field_utmnorthing;

%%
figure;
plot(one_place_utm_easting,one_place_utm_northing,'o')
xlabel('easting[m]');
ylabel('northing[m]');
title('stationary data')
figure;
plot(walking_utm_easting,walking_utm_northing,'o')
xlabel('easting[m]');
ylabel('northing[m]');
title('moving data');


%%
p = polyfit(walking_utm_easting,walking_utm_northing,1); 
f = polyval(p,walking_utm_easting);
%plot(x,y,'o',x,f,'-')
plot(walking_utm_easting,walking_utm_northing,'o',walking_utm_easting,f,'-')
legend('raw data','best fit line')
title('moving data');


%%

error = f - walking_utm_northing;
plot(walking_utm_easting,error)
title('northing error based on easting samples')
xlabel('easting samples[m]')
ylabel('northing error[m]')

%%
S_easting = std(one_place_utm_easting)
S_northing = std(one_place_utm_northing)
s_error = std(error)
