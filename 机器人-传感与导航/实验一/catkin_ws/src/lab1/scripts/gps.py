#!/usr/bin/env python2
# import sys
# sys.path.append('../')
import serial
import utm
# import sys
# sys.path.append("/opt/ros/melodic/lib/python2.7/dist-packages")
from lab1.msg import data_type
import rospy


def readloop():
    while True:
        line = ser.readline()
        print(line)
        splitline = line.split(',')
        if splitline[0] == '$GPGGA':
            latitude = splitline[2]
            # print(latitude)
            # print("yes")
            latDirec = splitline[3]
            # print(splitline[0])
            longitude = splitline[4]
            longDirc = splitline[5]
            altitude = splitline[9]
            # print(splitline)
            # print(latitude,longitude)
            lat = (int(float(latitude)/100) + (float(latitude) % 100)/60)
            lon = -(int(float(longitude)/100) + (float(longitude) % 100)/60)
            print(type(lat))
            # print(lat,lon)
            utm_conversion = utm.from_latlon(lat, lon)
            # utm_easting = utm_conversion[0]
            # utm_northing = utm_conversion[1]
            # utm_zone = utm_conversion[2]
            # utm_letter = utm_conversion[3]
            data = data_type()
            data.timestamp = float(splitline[1])
            # print(data.timestamp)
            data.latitude = lat
            data.lat_dir = latDirec
            data.longitude = lon
            data.long_dir = longDirc
            data.altitude = float(altitude)
            data.utmeasting = str(utm_conversion[0])
            print(type(data.utmeasting))
            data.utmnorthing = str(utm_conversion[1])
            data.zone = str(utm_conversion[2])
            data.letter = str(utm_conversion[3])
            print(data)
            # print(utm_easting, utm_northing, utm_zone, utm_letter)
            pub.publish(data)



if __name__ == "__main__":
    ser = serial.Serial('/dev/ttyUSB2', 4800, timeout=5)
    rospy.init_node('gps')
    pub = rospy.Publisher("/gps_data_transmission", data_type, queue_size=10)
    readloop()














