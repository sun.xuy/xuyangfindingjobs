clc; clear all;
close all;

% params;
% t = 0:0.05:10; 
% ang_speed = 4;
% theta = ang_speed*t;

n = 20;
theta = linspace(20,-20,n)*pi/180;


for i = 1:n 
  

    [p1(:,i),p2(:,i),p3(:,i),p4(:,i),q2(:,i)] = func_4bar(theta(i));
    
end

%%


% plot
filename = 'sim_mechanism.gif';
 figure;
for i=1:n 

    
    plot([p1(1,i) p1(1,i)],[p1(1,i) p1(1,i)],'o');
hold on
    plot([p2(1,i) p2(1,i)],[p2(2,i) p2(2,i)],'o');
    plot([p3(1,i) p3(1,i)],[p3(2,i) p3(2,i)],'o');
    plot([p4(1,i) p4(1,i)],[p4(2,i) p4(2,i)],'o');
    
   A_bar = plot([p1(1,i) p2(1,i)],[p1(2) p2(2,i)],'linewidth',2);
   B_bar = plot([p2(1,i) p3(1,i)],[p2(2,i) p3(2,i)],'linewidth',3);
   C_bar = plot([p3(1,i) p4(1,i)],[p3(2,i) p4(2,i)],'linewidth',4);
   hold off
   legend([A_bar B_bar C_bar ],'A','B','C')
   axis equal
   set(gca,'XLim',[-5 8],'YLim',[-5 7]);
   
        frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end


end


%%
figure;
plot(q2*180/pi);


