
function [q2] = func_FWDoptimization(theta,x)

gamma = atan2(x(5),x(4));         
D = sqrt(x(5)^2+x(4)^2);
p1_temp = [0;0;0];
p4_temp = D*[cos(gamma);sin(gamma);0];
p2_temp = x(1)*[cos(theta+gamma); sin(theta+gamma);0]; 
E = sqrt(x(1)^2 + D^2 - 2*x(1)*D*cos(theta));
alfa = asin(x(1)*sin(theta)/E);
beta = acos((E^2 + x(3)^2 - x(2)^2)/(2*E*x(3)));
p3_temp = p4_temp + x(3)*[-cos(alfa+beta-gamma);sin(alfa+beta-gamma);0 ];

p1 = p1_temp;
p2 = p2_temp;
p3 = p3_temp;
p4 = p4_temp;

gamma2 = acos((x(2)^2+E^2-x(3)^2)/(2*x(2)*E));
gamma3 = acos((x(1)^2+E^2-D^2)/(2*x(1)*E)); 
q2 = -(pi-gamma2-gamma3);



end