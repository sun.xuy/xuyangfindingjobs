params: parameters for sim_mechanism.m file. 

sim_mechanism.m : you can run your params in this file to test whether fmincon give you the correct params;

sim_optimization.m : in this file, you can run fmincon to obtain the params; later you can copy paste the params 
obtained from fmincon to params.m and then run sim_mechanism.m file.

notes for flapping mechanism.pdf explains all the geometry and equations.