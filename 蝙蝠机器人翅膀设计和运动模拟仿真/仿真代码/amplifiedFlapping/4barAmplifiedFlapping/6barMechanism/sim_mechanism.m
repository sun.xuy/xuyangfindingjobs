

clear all;
clc;
close all;

params;
% this is the angle that the cam arm starts to touch the bat frame
temp1 = l1/lr;
gamma1 = asin(temp1);

n = 50;
q1 = linspace(gamma1,pi-gamma1,n);

% calculate the position of each point in the flapping mechanism
for i = 1:n

    [pA(i,:),pE(i,:),p1(i,:),p2(i,:),p3(i,:),p4(i,:)...
        ,pb_R(i,:),pd_R(i,:),pc_R(i,:),pe_R(i,:),p_wing(i,:),q2(i,:)]= func_fwdFlapping(q1(i));

end
%%
% plot
figure;

filename = 'sim_bat.gif';
for i = 1:n

ground = plot([pA(i,1),pA(i,1)],[pA(i,2),pA(i,2)],'o','MarkerFaceColor','k');
hold on
plot([pE(i,1),pE(i,1)],[pE(i,2),pE(i,2)],'o');
plot([p1(i,1),p1(i,1)],[p1(i,2),p1(i,2)],'o','MarkerFaceColor','k');
plot([p2(i,1),p2(i,1)],[p2(i,2),p2(i,2)],'o');
plot([p3(i,1),p3(i,1)],[p3(i,2),p3(i,2)],'o');
plot([pd_R(i,1),pd_R(i,1)],[pd_R(i,2),pd_R(i,2)],'o');
plot([p4(i,1),p4(i,1)],[p4(i,2),p4(i,2)],'o','MarkerFaceColor','k');


plot([pA(i,1),pE(i,1)],[pA(i,2),pE(i,2)],'--');
plot([p2(i,1),pE(i,1)],[p2(i,2),pE(i,2)]);
plot([p2(i,1),p3(i,1)],[p2(i,2),p3(i,2)]);
plot([p3(i,1),p4(i,1)],[p3(i,2),p4(i,2)]);
plot([p2(i,1),p1(i,1)],[p2(i,2),p1(i,2)]);

% plot the wing
wing = plot([p4(i,1),p_wing(i,1)],[p4(i,2),p_wing(i,2)],'linewidth',3);

% plot the frame
cam = plot([pb_R(i,1),pd_R(i,1)],[pb_R(i,2),pd_R(i,2)],'linewidth',2);
motor = plot([pb_R(i,1),pb_R(i,1)],[pb_R(i,2),pb_R(i,2)],'o','MarkerFaceColor','b');
plot([pA(i,1),pc_R(i,1)],[pA(i,2),pc_R(i,2)]);
plot([pc_R(i,1),pe_R(i,1)],[pc_R(i,2),pe_R(i,2)]);


hold off
xlabel('x');
ylabel('y');
axis equal
xlim([-0.2 5]);
ylim([-0.2 5]);
% xlim([-2 4]);
% ylim([-2 4]);

title('bio-bat');
xlabel('x');
ylabel('y');
legend([ground cam motor wing],'Ground','Cam Arm','motor','wing');



box on
   frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end

end

%%

figure;
plot(q1*180/pi);
hold on
plot(q2*180/pi);
hold off
xlabel('n[time step]');
ylabel('angle[degree]');
xlim([-2 n+2]);
ylim([-90 180]);
legend('q1-input','q2-output');
title('joint trajectory')





