




% % params for the bat
% x1 = 0.6;   
% y1 = 0.7;
% x2 = 0.95;
% y2 = 0.75;
% 
% l1 = 0.2;
% l2 = 0.9;
% lh = 0.4;
% lr = 0.29;
% 
% b = 0.5;
% A = 0.4;
% B = 0.45;
% C = 0.2;
% % D = 0.4;
% % W stands for wing length
% W = 1;
% 
% % position of point 1 and point 2
% p1 = [x1 y1 0];
% p4 = [x2 y2 0];

%%
% parameters for testing the results of the optimizer
% x = [x1 y1 x2 y2...
%      l1 l2 lh lr...
%      b A B C D]
% % solution1:
% x1 = 0.7464;  %x(1)
% y1 = 0.9388;  %x(2)
% x2 = 1.7543;  %x(3)
% y2 = 1.2428;  %x(4)
% 
% l1 = 0.4267;  %x(5)
% l2 = 1.1725;  %x(6)
% lh = 0.9002;  %x(7)
% lr = 0.4655;  %x(8)
% 
% b = 0.6397;  %x(9)
% A = 0.6349;  %x(10)
% B = 1.0922;  %x(11)
% C = 0.2888;  %x(12)
% w1 = 1.6;    %x(13)
% l_wire = 7;%x(14)
% W = 3.5;%x(15)
% w0 = 1.5;%x(16)

% A1 = 1.9669;
% B1 = 2.8210;
% C1 = 1.9669;
% D1 = 2.8815;
% delta_wingX = 2.4956;
% delta_wingY = 2.4978;

% A1 = 3.0597; 
% B1 = 4.3817 ; 
% C1 = 3.0598; 
% D1 = 4.4760;
% 
% 
% delta_wingX = 0.0065;
% delta_wingY = 3.4313;

%%
% % % % % solution2 from fmincon
% x1 = 1.5423;  %x(1)
% y1 = 1.2500;  %x(2)
% x2 = 1.5607;  %x(3)
% y2 = 1.5523;  %x(4)
% 
% l1 = 0.5040;  %x(5)
% l2 = 1.3336;  %x(6)
% lh = 0.8448;  %x(7)
% lr = 0.5664;  %x(8)
% 
% b = 0.7155;  %x(9)
% A = 0.4270;  %x(10)
% B = 1.2634;  %x(11)
% C = 0.7474;  %x(12)
% 
% w1 = 01.7384;    %x(13)
% l_wire = 6.4523;%x(14)
% W = 2.7182;%x(15)
% w0 = 1.1417;%x(16)

% a = sqrt(l1^2+l2^2);
% d = sqrt(x1^2+y1^2);
% W stands for the length of the wing

% w2 = 0.4;
% 
% % l_wire_L = 5.58;
% l_wire_L = 5.59;

% alpha8 = atan2(y2-y1,x2-x1);
% alpha9 = atan2(y1,x1);
% p1 = [x1 y1 0];
% p4 = [x2 y2 0];
%%
% solution 3
x1 = 0.5760;  %x(1)
y1 = 0.7178;  %x(2)
x2 = 0.9966;  %x(3)
y2 = 0.7412;  %x(4)

l1 = 0.2040;  %x(5)
l2 = 0.9165;  %x(6)
lh = 0.4299;  %x(7)
lr = 0.3132;  %x(8)

b = 0.5005;  %x(9)
A = 0.4210;  %x(10)
B = 0.4312;  %x(11)
C = 0.1849;  %x(12)



% w1 = 1.6;    %x(13)
% l_wire = 7;%x(14)
% W = 3.5;%x(15)
% w0 = 1.5;%x(16)


% left side of robot
x1_l = -x1;
y1_l = y1;
x2_l = -x2;
y2_l = y2;

% alpha8_l = atan2(y2_l-y1_l,x2_l-x1_l);
% alpha9_l = atan2(y1_l,x1_l);
% p1_l = [x1_l y1_l 0];
% p4_l = [x2_l y2_l 0];

