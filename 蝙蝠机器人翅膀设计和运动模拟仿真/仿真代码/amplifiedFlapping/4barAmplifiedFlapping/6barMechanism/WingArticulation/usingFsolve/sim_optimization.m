

clear all;
close all;


% set up desired trajectory for q2
n = 20;
q_d2 = linspace(20,-20,n)*pi/180;
q_d3 = linspace(-40,-50,n)*pi/180;


% setup initial condition
x0 = [0.7464 0.9388 1.7543 1.2428...%x1 y1 x2 y2
    0.4267 1.1725 0.9002 0.4655...%l1 l2 lh lr
    0.6397 0.6349 1.0922 0.2888...%b A B C
    1.6 7 3.5 1.5];%w1 l_wire W(wing_arm_length) w0


% runing fmincon to obtain optimal params for the flapping mechanism
[x,J] = func_getParam(x0,q_d2,q_d3,n);





function [x,J] = func_getParam(x0,q_d2,q_d3,n)


% set up linear constraints
A = [];
b = [];
Aeq = [];
beq = [];
% set lower bound and upper bound
lb = [0.2 0.4 0.3 0.5 ...
    0.1 0.4 0.3 0.2 ...
    0.2 0.3 0.35 0.1...
    0.05 3 1 1];
ub = [2 2.2 2.5 2.3 ...
    1 2 1 2 ...
    1 2 2 1.5...
    2 10 8 8];



options = optimset('Display','off');
% run fmincon
[x,J] = fmincon(@func_cost,x0,A,b,Aeq,beq,lb,ub,@loopConstraint,options);




    function J = func_cost(x)
        %       this is the angle that the cam arm starts to touch the bat frame
        temp1 = x(5)/x(8);
        alpha_rotation = asin(temp1);
        %         this is the angle that the cam arm is perpendicular to the bat
        %         frame, which q2 can achieve maximum flapping motion
        q1_maxoutput = acos((x(8)-x(5))/x(7));
        %       input q1
        q1 = linspace(alpha_rotation,q1_maxoutput,n);
        x0 = -10*pi/180;
        for i = 1:n
            [q2(1,i),q3(1,i),gamma2] = func_FWDoptimization2(q1(i),x,alpha_rotation,x0);
            
        end
        %        use least square to calculate the cost
        J = (q_d3-q3)*(q_d3-q3).'+(q_d2-q2)*(q_d2-q2).';
    end



    function [c,ceq] = loopConstraint(x)
        
        %       this is the angle that the cam arm starts to touch the bat frame
        temp1 = x(5)/x(8);
        alpha_rotation = asin(temp1);
        %         this is the angle that the cam arm is perpendicular to the bat
        %         frame, which q2 can achieve maximum flapping motion
        q1_maxoutput = acos((x(8)-x(5))/x(7));
        %       input q1
        q1 = linspace(alpha_rotation,q1_maxoutput,n);
        
        
        %       running the kinematics to check q2, if q2 output complex number,
        %       then it should reject it because of the constrains I set for the
        %       nonlinear constrain in fmincon
        x0 = -10*pi/180;
        for i = 1:n
            [q2(1,i),q3(1,i),gamma2] = func_FWDoptimization2(q1(i),x,alpha_rotation,x0);
            
        end

        eqn3 = x(8)-x(7);
        eqn4 = x(5)-x(8);

        
        c = [eqn3,eqn4];

        a10 = double(imag(q3)~=0);
        ceq = [a10];

    end




end



























