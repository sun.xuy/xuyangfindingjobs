

clear all;



params;
% this is the angle that the cam arm starts to touch the bat frame
temp1 = l1/lr;
gamma1 = asin(temp1);

n = 20;
q1 = linspace(gamma1,pi-gamma1,n);

% calculate the position of each point in the flapping mechanism
for i = 1:n
    
    [   pA(i,:),pE(i,:),pE_wire(i,:),p1(i,:),p2(i,:),p3(i,:),p4(i,:),...
        pb_R(i,:),pd_R(i,:),pc_R(i,:),pe_R(i,:),p_wing(i,:),pw2(i,:),pW2(i,:),q2(i,:)...
        pE_L(i,:),pE_wire_L(i,:),p1_L(i,:),p2_L(i,:),p3_L(i,:),p4_L(i,:),...
        pb_L(i,:),pd_L(i,:),pc_L(i,:),pe_L(i,:),p_wing_L(i,:),pw2_L(i,:),pW2_L(i,:),pg_L(i,:),q2_L(i,:)...
        q3(i,:),q3_l(i,:)]= func_fwdFlapping(q1(i));
    
end
%%
% plot
figure;

filename = 'sim_bat2.gif';
for i = 1:n
    
    ground = plot([pA(i,1),pA(i,1)],[pA(i,2),pA(i,2)],'o','MarkerFaceColor','k');
    hold on
    plot([pE(i,1),pE(i,1)],[pE(i,2),pE(i,2)],'o');
    plot([p1(i,1),p1(i,1)],[p1(i,2),p1(i,2)],'o','MarkerFaceColor','k');
    plot([p2(i,1),p2(i,1)],[p2(i,2),p2(i,2)],'o');
    plot([p3(i,1),p3(i,1)],[p3(i,2),p3(i,2)],'o');
    plot([pd_R(i,1),pd_R(i,1)],[pd_R(i,2),pd_R(i,2)],'o');
    plot([p4(i,1),p4(i,1)],[p4(i,2),p4(i,2)],'o','MarkerFaceColor','k');
    
    plot([pE_L(i,1),pE_L(i,1)],[pE_L(i,2),pE_L(i,2)],'o');
    plot([p1_L(i,1),p1_L(i,1)],[p1_L(i,2),p1_L(i,2)],'o','MarkerFaceColor','k');
    plot([p2_L(i,1),p2_L(i,1)],[p2_L(i,2),p2_L(i,2)],'o');
    plot([p3_L(i,1),p3_L(i,1)],[p3_L(i,2),p3_L(i,2)],'o');
    plot([pd_L(i,1),pd_L(i,1)],[pd_L(i,2),pd_L(i,2)],'o');
    plot([p4_L(i,1),p4_L(i,1)],[p4_L(i,2),p4_L(i,2)],'o','MarkerFaceColor','k');
    
    plot([pA(i,1),pE(i,1)],[pA(i,2),pE(i,2)],'--');
    plot([p2(i,1),pE(i,1)],[p2(i,2),pE(i,2)]);
    plot([p2(i,1),p3(i,1)],[p2(i,2),p3(i,2)]);
    plot([p3(i,1),p4(i,1)],[p3(i,2),p4(i,2)]);
    plot([p2(i,1),p1(i,1)],[p2(i,2),p1(i,2)]);
%     plot([pE(i,1),pE_wire(i,1)],[pE(i,2),pE_wire(i,2)]);
    
    plot([pA(i,1),pE_L(i,1)],[pA(i,2),pE_L(i,2)],'--');
    plot([p2_L(i,1),pE_L(i,1)],[p2_L(i,2),pE_L(i,2)]);
    plot([p2_L(i,1),p3_L(i,1)],[p2_L(i,2),p3_L(i,2)]);
    plot([p3_L(i,1),p4_L(i,1)],[p3_L(i,2),p4_L(i,2)]);
    plot([p2_L(i,1),p1_L(i,1)],[p2_L(i,2),p1_L(i,2)]);
%     plot([pE_L(i,1),pE_wire_L(i,1)],[pE_L(i,2),pE_wire_L(i,2)]);
    
    % plot the wing
    wing = plot([p4(i,1),p_wing(i,1)],[p4(i,2),p_wing(i,2)],'linewidth',3);
    plot([pw2(i,1),p_wing(i,1)],[pw2(i,2),p_wing(i,2)]);
    %     plot([pw2(i,1),pg(i,1)],[pw2(i,2),pg(i,2)]);
    %     plot([pE_wire_L(i,1),pg(i,1)],[pE_wire_L(i,2),pg(i,2)]);
    plot([pw2(i,1),pW2(i,1)],[pw2(i,2),pW2(i,2)]);
    plot([pE_L(i,1),pw2(i,1)],[pE_L(i,2),pw2(i,2)]);
    
    plot([p4_L(i,1),p_wing_L(i,1)],[p4_L(i,2),p_wing_L(i,2)],'linewidth',3);
%     plot([pw2_L(i,1),p_wing_L(i,1)],[pw2_L(i,2),p_wing_L(i,2)]);
%     plot([pw2_L(i,1),pg_L(i,1)],[pw2_L(i,2),pg_L(i,2)]);
%     plot([pE_wire(i,1),pg_L(i,1)],[pE_wire(i,2),pg_L(i,2)]);
%     plot([pw2_L(i,1),pW2_L(i,1)],[pw2_L(i,2),pW2_L(i,2)]);
    
    
    
    % plot the frame
    cam = plot([pd_L(i,1),pd_R(i,1)],[pd_L(i,2),pd_R(i,2)],'linewidth',2);
    motor = plot([pb_R(i,1),pb_R(i,1)],[pb_R(i,2),pb_R(i,2)],'o','MarkerFaceColor','b');
    plot([pA(i,1),pc_R(i,1)],[pA(i,2),pc_R(i,2)]);
    plot([pc_R(i,1),pe_R(i,1)],[pc_R(i,2),pe_R(i,2)]);
    plot([pA(i,1),pc_L(i,1)],[pA(i,2),pc_L(i,2)]);
    plot([pc_L(i,1),pe_L(i,1)],[pc_L(i,2),pe_L(i,2)]);
    
    hold off
    xlabel('x');
    ylabel('y');
    axis equal
    xlim([-8 8]);
    ylim([-4 7]);
    % xlim([-2 4]);
    % ylim([-2 4]);
    
    title('bio-bat');
    xlabel('x');
    ylabel('y');
    legend([ground cam motor wing],'Ground','Cam Arm','motor','wing');
    
    
    
    box on
    frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    
end

%%

figure;
plot(q1*180/pi);
hold on
plot(q3*180/pi);
hold off
xlabel('n[time step]');
ylabel('angle[degree]');
xlim([-2 n+2]);
ylim([-180 180]);
legend('q1-input','q_3');
title('joint trajectory');





