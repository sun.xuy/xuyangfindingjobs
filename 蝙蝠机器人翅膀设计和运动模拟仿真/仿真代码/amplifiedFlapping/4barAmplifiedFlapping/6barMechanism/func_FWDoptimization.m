
function [q2] = func_FWDoptimization(q1,x)


% length and angles from the sketch
a = sqrt(x(5)^2+x(6)^2);
d = sqrt(x(1)^2+x(2)^2);
D = sqrt((x(4)-x(2))^2+(x(3)-x(1))^2);
alpha8 = atan2(x(4)-x(2),x(3)-x(1));
alpha9 = atan2(x(2),x(1));


% please refer to notes
% angles and length from the bat frame
% lad is the length from point A to D
% l2a is the length from point C to D
lad = sqrt(x(7)^2+x(8)^2-2*x(7)*x(8)*cos(q1));
l2a = sqrt(lad^2-x(5)^2);

alpha2 = acos((lad^2+x(7)^2-x(8)^2)/(2*lad*x(7)));
alpha4 = acos((lad^2+x(5)^2-l2a^2)/(2*lad*x(5)));
alpha6 = pi-alpha2-alpha4;

% position of point E
pex1 = x(5)*sin(alpha6)+x(6)*cos(alpha6);
pey1 = -x(5)*cos(alpha6)+x(6)*sin(alpha6);
% pE = [pex1 pey1 0]';

% find theta1, which is a function of input q1
theta1 = acos(pex1/a)-alpha9;


% first 4 bar mechanism
% please refer to notes
% e is the length from point E to point 1

e = sqrt(a^2 + d^2 - 2*a*d*cos(theta1));
alfa1 = asin(a*sin(theta1)/e);
beta1 = acos((e^2 + x(10)^2 - x(9)^2)/(2*x(10)*e));

% find theta2, which is a function of theta1, meaning that theta2 is also a
% function of input q1
theta2 = pi-alpha8-alfa1-beta1+alpha9;

% second 4 bar mechanism
% please refer to notes
% l_E is the length from point 2 to point 4
l_E = sqrt(x(10)^2 + D^2 - 2*x(10)*D*cos(theta2));
alfa2 = asin(x(10)*sin(theta2)/l_E);
beta2 = acos((l_E^2 + x(12)^2 - x(11)^2)/(2*l_E*x(12)));

% calculate the position of each point in bat frame
% point E,B,D,C on the bat frame
pe_R = [pex1 pey1 0]';
pb_R = [0 x(7) 0]';
pd_R = pb_R + [x(8)*sin(q1) -x(8)*cos(q1) 0].';
pc_R = [x(5)*sin(alpha6) -x(5)*cos(alpha6) 0]';

% calculate the position of each point in first four bar mechanism
pE = [pex1 pey1 0]';
pA = [0 0 0].';
p1 = [x(1) x(2) 0].';
p2 = [x(1)+x(10)*cos(theta2+alpha8) x(2)+x(10)*sin(theta2+alpha8) 0].';

% calculate the position of each point in second four bar mechanism
% p1 = [x(1) x(2) 0].';
% p2 = [x(1)+x(10)*cos(theta2+alpha8) x(2)+x(10)*sin(theta2+alpha8) 0].';
p4 = [x(3) x(4) 0].';
p3 = p4+ [-x(11)*cos(alfa2+beta2-alpha8),x(11)*sin(alfa2+beta2-alpha8),0].';
% p4 = [x(1)+x(12)*cos(alpha8) x(2)+x(12)*sin(alpha8) 0].';


% position of the wing
% assume length of the wing is 1 meter
W = 1;
p_wing = p4+[-W*cos(alfa2+beta2-alpha8),W*sin(alfa2+beta2-alpha8),0].';



% output q2
q2 = pi-(alfa2+beta2-alpha8);
                
                
end












