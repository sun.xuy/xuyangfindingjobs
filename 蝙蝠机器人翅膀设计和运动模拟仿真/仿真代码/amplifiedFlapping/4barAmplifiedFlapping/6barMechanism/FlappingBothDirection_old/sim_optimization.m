

clear all;
close all;


% set up desired trajectory for q2
n = 20;
q_d2 = linspace(10,-10,n)*pi/180;
q_d3 = linspace(-5,-40,n)*pi/180;


% setup initial condition
x0 = [0.7464 0.9388 1.7543 1.2428...%x1 y1 x2 y2
    0.4267 1.1725 0.9002 0.4655...%l1 l2 lh lr
    0.6397 0.6349 1.0922 0.2888...%b A B C
    0.5 5.68 3.5 0.4 0.4];%w1 l_wire w_length(the wing length) le_extended w2


% runing fmincon to obtain optimal params for the flapping mechanism
[x,J] = func_getParam(x0,q_d2,q_d3,n);





function [x,J] = func_getParam(x0,q_d2,q_d3,n)


% set up linear constraints
A = [];
b = [];
Aeq = [];
beq = [];
% set lower bound and upper bound
lb = [0.2 0.4 0.3 0.5 ...
    0.1 0.4 0.3 0.2 ...
    0.2 0.3 0.35 0.1...
    0.05 3 2 0.1 0.1];
ub = [2 2.2 2.5 2.3 ...
    1 2 1 2 ...
    1 2 2 1.5...
    2  10 15 3 3 ];



options = optimset('Display','off');
% run fmincon
[x,J] = fmincon(@func_cost,x0,A,b,Aeq,beq,lb,ub,@loopConstraint,options);




    function J = func_cost(x)
        %       this is the angle that the cam arm starts to touch the bat frame
        temp1 = x(5)/x(8);
        theta1 = asin(temp1);
        %         this is the angle that the cam arm is perpendicular to the bat
        %         frame, which q2 can achieve maximum flapping motion
        q1_maxoutput = acos((x(8)-x(5))/x(7));
        %       input q1
        q1 = linspace(theta1,q1_maxoutput,n);
        
        for i = 1:n
            [q2(1,i),q3(1,i)] = func_FWDoptimization2(q1(i),x);
        end
        %        use least square to calculate the cost
        J = (q_d2-q2)*(q_d2-q2).'+(q_d3-q3)*(q_d3-q3).';
        
    end



    function [c,ceq] = loopConstraint(x)
        
        %       this is the angle that the cam arm starts to touch the bat frame
        temp1 = x(5)/x(8);
        theta1 = asin(temp1);
        %         this is the angle that the cam arm is perpendicular to the bat
        %         frame, which q2 can achieve maximum flapping motion
        q1_maxoutput = acos((x(8)-x(5))/x(7));
        %       input q1
        q1 = linspace(theta1,q1_maxoutput,n);
        
        
        %       running the kinematics to check q2, if q2 output complex number,
        %       then it should reject it because of the constrains I set for the
        %       nonlinear constrain in fmincon
        for i = 1:n
            [q2(1,i),alpha2(1,i),alpha4(1,i)...
                ,alfa1(1,i),beta1(1,i),alfa2(1,i),beta2(1,i),...
                alpha2_l(1,i),alpha4_l(1,i),temp1_angle(1,i),temp1(1,i)] = func_FWDoptimization(q1(i),x);
        end
        
        
        %         make the output q2 to be in the range of 90 degree to -90 degree
        %         eqn1 = q2-pi/2;
        %         eqn2 = -q2-pi/2;
        %       set constrains to make the cam arm less than the pivot position(B)
        
        eqn3 = x(8)-x(7);
        %       set constrains to make l1 less than cam arm
        eqn4 = x(5)-x(8);
        %         c = [eqn1;eqn2];

        c = [eqn3,eqn4];
        %         if q2 is a complex number, fmincon will reject the params;
        a1 = double(imag(q2)~=0);
%         a2= double(imag(alpha2)~=0);
%         a3 = double(imag(alpha4)~=0);
%         a4 = double(imag(alfa1)~=0);
%         a5 = double(imag(beta1)~=0);
%         a6 = double(imag(alfa2)~=0);
%         a7 = double(imag(beta2)~=0);
%         a8 = double(imag(alpha2_l)~=0);
%         a9 = double(imag(alpha4_l)~=0);
        a10 = double(imag(temp1_angle)~=0);


        
        ceq = [a1;a10];
        
        
        %         set up legnth constrains for 2 four bar double crank mechanism
        %             eqn1 =    x(9)+sqrt(x(1)^2+x(2)^2)-sqrt(x(5)^2+x(6)^2)-x(10);
        %             eqn2 = - (x(10)+sqrt(x(1)^2+x(2)^2)-sqrt(x(5)^2+x(6)^2)-x(9));
        %             eqn3 =    x(9)+x(10)-sqrt(x(5)^2+x(6)^2)-sqrt(x(1)^2+x(2)^2);
        %
        %             eqn4 =     x(11)+x(13)-x(10)-x(12);
        %             eqn5 =  - (x(12)+x(13)-x(10)-x(11));
        %             eqn6 =     x(11)+x(12)-x(10)-x(13);
        
        %             eqn7 = x(8)-x(7);
        %             c = [eqn1;eqn2;eqn3;eqn4;eqn5;eqn6;eqn7];
        
        
        
        %         ceq = [];
    end




end



























