




% % params for the bat
% x1 = 0.6;   
% y1 = 0.7;
% x2 = 0.95;
% y2 = 0.75;
% 
% l1 = 0.2;
% l2 = 0.9;
% lh = 0.4;
% lr = 0.29;
% 
% b = 0.5;
% A = 0.4;
% B = 0.45;
% C = 0.2;
% % D = 0.4;
% % W stands for wing length
% W = 1;
% 
% % position of point 1 and point 2
% p1 = [x1 y1 0];
% p4 = [x2 y2 0];


% parameters for testing the results of the optimizer
% x = [x1 y1 x2 y2...
%      l1 l2 lh lr...
%      b A B C D]
% [70 -40]
% x1 = 0.9713;  %x(1)
% y1 = 1.1007;  %x(2)
% x2 = 1.5549;  %x(3)
% y2 = 1.4990;  %x(4)
% 
% l1 = 0.2734;  %x(5)
% l2 = 1.4464;  %x(6)
% lh = 0.8362;  %x(7)
% lr = 0.6018;  %x(8)
% 
% b = 0.7015;  %x(9)
% A = 0.6819;  %x(10)
% B = 0.9985;  %x(11)
% C = 0.7654;  %x(12)

% [70 -70]
x1 = 0.5760;  %x(1)
y1 = 0.7178;  %x(2)
x2 = 0.9966;  %x(3)
y2 = 0.7412;  %x(4)

l1 = 0.2040;  %x(5)
l2 = 0.9165;  %x(6)
lh = 0.4299;  %x(7)
lr = 0.3132;  %x(8)

b = 0.5005;  %x(9)
A = 0.4210;  %x(10)
B = 0.4312;  %x(11)
C = 0.1849;  %x(12)


a = sqrt(l1^2+l2^2);
d = sqrt(x1^2+y1^2);
% W stands for the length of the wing
W = 2.5;

alpha8 = atan2(y2-y1,x2-x1);
alpha9 = atan2(y1,x1);
p1 = [x1 y1 0];
p4 = [x2 y2 0];


