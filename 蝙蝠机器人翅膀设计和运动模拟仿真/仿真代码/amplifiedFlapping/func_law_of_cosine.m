
% obtain cos(theta) based on given the lengths of a triangle
%
% input:
%   C: dim of line in front of theta
%   A, B: dim of lines adjacent to theta
%
% output:
%   q: cos(theta)
%
% By Xuyang Sun
function q = func_law_of_cosine(C,A,B)

q = (-C^2+A^2+B^2)/(2*A*B);

end