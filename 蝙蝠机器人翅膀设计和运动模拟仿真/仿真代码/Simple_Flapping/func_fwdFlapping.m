function [pa_R,pb_R,pc_R,pd_frame_R,pd_R,pe_R,pf_R...
          pa_L,pb_L,pc_L,pd_frame_L,pd_L,pe_L,pf_L  ] = func_fwdFlapping(q1)



params;


% define transformation matrix

Tx = @(x,y,z,q) [1      0        0    x;...
                 0    cos(q)   sin(q) y;...
                 0    -sin(q)  cos(q) z;...
                 0      0        0    1];

Ty = @(x,y,z,q) [cos(q)   0   sin(q)  x;...
                    0    1       0   y ;...
               -sin(q)   0   cos(q)  z ;...
                    0    0       0   1 ];
                
Tz = @(x,y,z,q) [cos(q)   -sin(q)   0   x;...
                 sin(q)    cos(q)   0   y ;...
                    0       0       1   z ;...
                    0       0       0   1 ];


% calculate all the angles in the bat model
%%

q1 = mod(q1,2*pi);


temp1 = l1_R/l3_R;
theta1 = asin(temp1);

temp2 = l1_L/l3_L;
theta2 = asin(temp2);


% cam does not touch the bio-bat frame for only one cam bar
if q1 <= theta1 || q1 >= pi-theta1 && q1 <= pi+theta2 || q1 <= 2*pi && q1 >= 2*pi-theta2
    
 
    
% kinematics of right side of the robot   

    
Tab_R = Tz(0,-lh,0,0);
Tbc_R = Tz(l1_R,0,0,0);
Tce_R = Tz(0,l2_R,0,0);
Tef_R = Tz(l4_R,0,0,0);

Tad_pr_R = Tz(0,0,0,q1);
Td_prd_R = Tz(0,-l3_R,0,0);


Pf_R = Tab_R*Tbc_R*Tce_R*Tef_R;
Pe_R = Tab_R*Tbc_R*Tce_R;
Pd_R = Tad_pr_R*Td_prd_R;
Pc_R = Tab_R*Tbc_R;
Pb_R = Tab_R;


pf_R = Pf_R(1:3,4);
pe_R = Pe_R(1:3,4);
pd_R = Pd_R(1:3,4);
pd_frame_R = [0,0,0];
pc_R = Pc_R(1:3,4);
pb_R = Pb_R(1:3,4);
pa_R = [0,0,0];


% kinematics of left side of the robot   


Tab_L = Tz(0,-lh,0,0);
Tbc_L = Tz(-l1_L,0,0,0);
Tce_L = Tz(0,l2_L,0,0);
Tef_L = Tz(-l4_L,0,0,0);
Taa_pr_L = Tz(0,0,0,q1);
Ta_prd = Tz(0,l3_L,0,0);

Pf_L = Tab_L*Tbc_L*Tce_L*Tef_L;
Pe_L = Tab_L*Tbc_L*Tce_L;
Pc_L = Tab_L*Tbc_L;
Pb_L = Tab_L;
Pd_L = Taa_pr_L*Ta_prd;


pf_L = Pf_L(1:3,4);
pe_L = Pe_L(1:3,4);
pd_frame_L = [0 0 0];
pd_L = Pd_L(1:3,4) ;
pc_L = Pc_L(1:3,4);
pb_L = Pb_L(1:3,4);
pa_L = [0,0,0];


elseif q1 > theta1 && q1< pi-theta1 
% kinematics of right side of the robot 

% angles for right half of the bat
alpha1_R = q1;
lbd_R = sqrt(l3_R^2+lh^2-2*l3_R*lh*cos(alpha1_R));


% triangle ADB, 3rd is q1
[alpha2_R,alpha3_R,~] = func_triangle_angles(l3_R,lh,lbd_R);

% triangle BCD
l2a_R = sqrt(lbd_R^2-l1_R^2);
l2b_R = l2_R-l2a_R;
[alpha4_R,alpha5_R,~] = func_triangle_angles(l2a_R,l1_R,lbd_R);


Tab_R = Tz(0,-lh,0,-(alpha2_R+alpha4_R));
Tbc_R = Tz(0,l1_R,0,pi/2);
Tcd_frame_R = Tz(0,l2a_R,0,0);
Td_framee_R = Tz(0,l2b_R,0,-pi/2);
Tef_R = Tz(0,l4_R,0,0);
Tcd_frame_pr_R = Tz(0,l2a_R,0,pi-alpha3_R-alpha5_R);
Td_frame_pra_R = Tz(0,l3_R,0,-q1);



Pf_R = Tab_R*Tbc_R*Tcd_frame_R*Td_framee_R*Tef_R;
Pe_R = Tab_R*Tbc_R*Tcd_frame_R*Td_framee_R;
Pd_frame = Tab_R*Tbc_R*Tcd_frame_R;
Pc_R = Tab_R*Tbc_R;
Pb_R = Tab_R;

pf_R = Pf_R(1:3,4);
pe_R = Pe_R(1:3,4);
pd_frame_R = Pd_frame(1:3,4);
pd_R = pd_frame_R;
pc_R = Pc_R(1:3,4);
pb_R = Pb_R(1:3,4);
pa_R = [0,0,0];

% kinematics of left side of the robot 

alpha1_L = pi-q1;
lbd_L = sqrt(l3_L^2+lh^2-2*l3_L*lh*cos(alpha1_L));


% triangle ADB, 3rd is alpha1_L
[alpha2_L,alpha3_L,~] = func_triangle_angles(l3_L,lh,lbd_L);

% triangle BCD
l2a_L = sqrt(lbd_L^2-l1_L^2);
l2b_L = l2_L-l2a_L;
[alpha4_L,alpha5_L,~] = func_triangle_angles(l2a_L,l1_L,lbd_L);


Tab_L = Tz(0,-lh,0,(alpha2_L+alpha4_L));
Tbc_L = Tz(0,l1_L,0,-pi/2);
Tcd_frame_L = Tz(0,l2a_L,0,0);
Td_framee_L = Tz(0,l2b_L,0,pi/2);
Tef_L = Tz(0,l4_L,0,0);
Tcd_frame_pr_L = Tz(0,l2a_L,0,pi-alpha3_L-alpha5_L);
Td_frame_pra_L = Tz(0,l3_L,0,-q1);
Taa_pr_L = Tz(0,0,0,q1);
Ta_prd = Tz(0,l3_L,0,0);


Pf_L = Tab_L*Tbc_L*Tcd_frame_L*Td_framee_L*Tef_L;
Pe_L = Tab_L*Tbc_L*Tcd_frame_L*Td_framee_L;
Pd_frame_L = Tab_L*Tbc_L*Tcd_frame_L;
Pc_L = Tab_L*Tbc_L;
Pb_L = Tab_L;
Pd_L = Taa_pr_L*Ta_prd;

pf_L = Pf_L(1:3,4);
pe_L = Pe_L(1:3,4);
pd_frame_L = Pd_frame_L(1:3,4);
pd_L = Pd_L(1:3,4);;
pc_L = Pc_L(1:3,4);
pb_L = Pb_L(1:3,4);
pa_L = [0,0,0];

else q1 > pi+theta2 && q1 < 2*pi-theta2
    
% forward kinematics of right half of the robot

    
alpha1_R = q1-pi;
lbd_R = sqrt(l3_R^2+lh^2-2*l3_R*lh*cos(alpha1_R));


% triangle ADB, 3rd is q1
[alpha2_R,alpha3_R,~] = func_triangle_angles(l3_R,lh,lbd_R);

% triangle BCD
l2a_R = sqrt(lbd_R^2-l1_R^2);
l2b_R = l2_R-l2a_R;
[alpha4_R,alpha5_R,~] = func_triangle_angles(l2a_R,l1_R,lbd_R);


Tab_R = Tz(0,-lh,0,-(alpha2_R+alpha4_R));
Tbc_R = Tz(0,l1_R,0,pi/2);
Tcd_frame_R = Tz(0,l2a_R,0,0);
Td_framee_R = Tz(0,l2b_R,0,-pi/2);
Tef_R = Tz(0,l4_R,0,0);
Tcd_frame_pr_R = Tz(0,l2a_R,0,pi-alpha3_R-alpha5_R);
Td_frame_pra_R = Tz(0,l3_R,0,-q1);



Pf_R = Tab_R*Tbc_R*Tcd_frame_R*Td_framee_R*Tef_R;
Pe_R = Tab_R*Tbc_R*Tcd_frame_R*Td_framee_R;
Pd_frame = Tab_R*Tbc_R*Tcd_frame_R;
Pc_R = Tab_R*Tbc_R;
Pb_R = Tab_R;

pf_R = Pf_R(1:3,4);
pe_R = Pe_R(1:3,4);
pd_frame_R = Pd_frame(1:3,4);
pd_R = pd_frame_R;
pc_R = Pc_R(1:3,4);
pb_R = Pb_R(1:3,4);
pa_R = [0,0,0];
    
    
    
% forward kinematics of left half of the robot
% angles for left half of the bat

% l2a_L = lh-l3_L*cos(theta2);
% l2b_L = l2_R-l2a_L;

alpha1_L = 2*pi-q1;
lbd_L = sqrt(l3_L^2+lh^2-2*l3_L*lh*cos(alpha1_L));


% triangle ADB, 3rd is alpha1_L
[alpha2_L,alpha3_L,~] = func_triangle_angles(l3_L,lh,lbd_L);

% triangle BCD
l2a_L = sqrt(lbd_L^2-l1_L^2);
l2b_L = l2_L-l2a_L;
[alpha4_L,alpha5_L,~] = func_triangle_angles(l2a_L,l1_L,lbd_L);


Tab_L = Tz(0,-lh,0,(alpha2_L+alpha4_L));
Tbc_L = Tz(0,l1_L,0,-pi/2);
Tcd_frame_L = Tz(0,l2a_L,0,0);
Td_framee_L = Tz(0,l2b_L,0,pi/2);
Tef_L = Tz(0,l4_L,0,0);
Tcd_frame_pr_L = Tz(0,l2a_L,0,pi-alpha3_L-alpha5_L);
Td_frame_pra_L = Tz(0,l3_L,0,-q1);


Pf_L = Tab_L*Tbc_L*Tcd_frame_L*Td_framee_L*Tef_L;
Pe_L = Tab_L*Tbc_L*Tcd_frame_L*Td_framee_L;
Pd_frame_L = Tab_L*Tbc_L*Tcd_frame_L;
Pc_L = Tab_L*Tbc_L;
Pb_L = Tab_L;

pf_L = Pf_L(1:3,4);
pe_L = Pe_L(1:3,4);
pd_frame_L = Pd_frame_L(1:3,4);
pd_L = pd_frame_L;
pc_L = Pc_L(1:3,4);
pb_L = Pb_L(1:3,4);
pa_L = [0,0,0];
    


end
% 

















end













